#define _CRT_SECURE_NO_WARNINGS

#include "SList.h"

void test1()
{
	SLTNode* plist = NULL;
	SLPushFront(&plist, 1);
	SLPushFront(&plist, 2);
	SLPushFront(&plist, 3);
	SLTprint(plist);

	SLTNode* pos = SLFind(plist, 2);

	SListInsertBefore(&plist, pos, 30);
	SLTprint(plist);
}

//void test1()
//{
//	SLTNode* plist = NULL;
//	SLPushFront(&plist, 1);
//	SLPushFront(&plist, 2);
//	SLPushFront(&plist, 3);
//	SLTprint(plist);
//
//	SLTNode* pos = SLFind(plist, 2);
//
//	SListInsertBefore(&plist, pos, 30);
//	SLTprint(plist);
//}

int main()
{
	test1();
	return 0;
}