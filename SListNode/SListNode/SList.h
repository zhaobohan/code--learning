#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTDataType;

typedef struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
}SLTNode;

void SLTprint(SLTNode* phead);

void SLPushFront(SLTNode** pphead, SLTDataType x);

void SLPushBack(SLTNode** pphead, SLTDataType x);

void SLPopBack(SLTNode** pphead);

void SLPopFront(SLTNode** pphead);

SLTNode* SLFind(SLTNode* phead, SLTDataType x);

void SListInsertBefore(SLTNode** pphead, SLTNode* pos, SLTDataType x);

void SListInsertAfter(SLTNode* pos, SLTDataType x);

void SListDestroy(SLTNode** pphead);