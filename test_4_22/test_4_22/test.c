#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <math.h>


//交换函数
//
//void Swap(int* a, int* b)
//{
//	int tmp= *a;
//	*a = *b;
//	*b = tmp;
//}
//
//int main()
//{
//	int a, b;
//	scanf("%d %d", &a, &b);
//	Swap(&a, &b);
//	printf("%d %d", a, b);
//}




//写一个函数，每调用一次这个函数，就会将 num 的值增加1。

//void add(int* num)
//{
//	(* num)++;
//}
//
//int main()
//{
//	int num = 0;
//	int* pn = &num;
//	for (int i = 0; i < 5; i++)
//	{
//		add(pn);
//	}
//	printf("num=%d", num);
//	return 0;
//}


//写一个函数，实现一个整形有序数组的二分查找

//int binary_search(int arr[], int k, int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		int mid = left + (right - left) / 2;    //避免溢出
//		if (arr[mid] > k)
//		{
//			right = mid-1;
//		}
//		else if (arr[mid] < k)
//		{
//			left = mid+1;
//		}
//		else
//			return mid;
//	}
//	return -1;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int k = 0;
//	scanf("%d", &k);
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = binary_search(arr, k, sz);
//	if (ret != -1)
//		printf("ok->%d", ret);
//	else
//		printf("nothing");
//}


//写一个函数判断一年是不是闰年

//int is_leap_year(int i)
//{
//	return ((i % 4 == 0) && (i % 100 != 0)) || (i % 400 == 0);
//}
//
//
//int main()
//{
//	int count = 0;
//	for (int i = 0; i <= 2000; i++)
//	{
//		if (is_leap_year(i) == 1)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\ncount=%d\n", count);
//	return 0;
//}



//写一个函数可以判断一个数是不是素数。

//int is_prime(int j)
//{
//	for (int i = 2; i < sqrt(j); i++)
//	{
//		if (j % i == 0)
//			return 0;
//	}
//	return 1;
//}
//
//int main()
//{
//	int count = 0;
//	for (int i = 100; i <= 200; i++)
//	{
//		if (is_prime(i) == 1)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\ncount=%d\n", count);
//	return 0;
//}



//int main()
//{
//	int arr[10] = { 1,2,3,4,5 };
//	char str[] = {"wa wa"};
//	int len1 = 0;
//	int len2 = 0;
//	len1 = sizeof(arr);
//	len2 = sizeof(str);
//	printf("len1=%d\nlen2=%d", len1, len2);
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	int len1 = 0;
//	int len2 = 0;
//	len1 = sizeof(arr) / sizeof(arr[0]);
//	len2 = strlen(arr);
//	printf("len1=%d\nlen2=%d", len1, len2);
//	return 0;
//}


//int main()
//{
//	int arr[20]={1,2,3,4,5};
//	char str[20]={"hello"};
//	return 0;
//}