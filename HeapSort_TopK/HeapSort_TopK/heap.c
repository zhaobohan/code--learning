#define _CRT_SECURE_NO_WARNINGS

#include "heap.h"

void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	assert(hp);
	hp->capacity = n;
	a = (HPDataType*)malloc(sizeof(HPDataType) * hp->capacity);
	if (a == NULL)
	{
		perror("malloc fail");
		return;
	}
	hp->a = a;
	hp->size = 0;
}

void HeapDestory(Heap* hp)
{
	assert(hp);
	hp->capacity = 0;
	hp->size = 0;
	free(hp->a);
	hp->a = NULL;
}

void Swap(HPDataType* c, HPDataType* p)
{
	HPDataType tmp = *c;
	*c = *p;
	*p = tmp;
}

//小堆
//堆顶元素是最小值
void AdjustUp(HPDataType* a, int child)
{
	assert(a);
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

//小堆
//堆顶元素是最小值
void AdjustDown(HPDataType* a, int n, int parent)
{
	assert(a);
	int child = parent * 2 + 1;
	while (child < n)
	{
		//确定child的值
		if (child + 1 < n && a[child] > a[child + 1])
		{
			child++;
		}
		//排成堆的规则
		if (a[parent] > a[child])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//检查容量 避免满了无法插入数据
	if (hp->size == hp->capacity)
	{
		int newcapacity = hp->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(hp->a, sizeof(HPDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		hp->a = tmp;
	}
	//插入数据
	hp->a[hp->size] = x;
	hp->size++;
	//排成堆的规则
	AdjustUp(hp->a, hp->size - 1);
}

void HeapPop(Heap* hp)
{
	assert(hp);
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	//排成堆的规则
	AdjustDown(hp->a, hp->size, 0);
}

HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->a[0];
}

int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

int HeapEmpty(Heap* hp)
{
	assert(hp);
	if (hp->capacity == 0)
	{
		return 1;
	}
	return 0;
}

void HeapSort(HPDataType* a, int size)
{
	assert(a);

	//建堆
	for (int i = (size - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, size, i);
	}
	
	//排序
	int end = size - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}