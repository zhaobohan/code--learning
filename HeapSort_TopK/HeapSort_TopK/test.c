#define _CRT_SECURE_NO_WARNINGS

#include "heap.h"

void CreateData()
{
	int n = 10000;
	srand((unsigned int)time(0));
	FILE* pf = fopen("test.txt", "w");
	if (pf == NULL)
	{
		perror("fopen fail");
		return;
	}
	for (int i = 0; i < n; i++)
	{
		int x = rand() % 10000;
		fprintf(pf, "%d\n", x);
	}
	fclose(pf);
}

void PrintTopK()
{
	Heap hp = { 0,0,0 };
	HeapCreate(&hp,hp.a,4);
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL)
	{
		perror("fopen fail");
		return;
	}
	int* kmaxheap = (int*)malloc(sizeof(int) * 5);
	if (kmaxheap == NULL)
	{
		perror("malloc fail");
		return;
	}
	for (int i = 0; i < 5; i++)
	{
		fscanf(pf, "%d", &kmaxheap[i]);
		HeapPush(&hp, kmaxheap[i]);
	}
	int val = 0;
	while (!feof(pf))
	{
		fscanf(pf, "%d", &val);
		if (val > kmaxheap[0])
		{
			kmaxheap[0] = val;
			AdjustDown(kmaxheap, 5, 0);
		}
	}
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", kmaxheap[i]);
	}
}

int main()
{
	//CreateData();
	PrintTopK();
	return 0;
}

//int main()
//{
//	Heap hp={0,0,0};
//	HeapCreate(&hp,hp.a,4);
//	int arr[] = { 10,20,40,30,1,2,4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//		printf("%d ", arr[i]);
//	printf("\n");
//	HeapSort(arr, sz);
//	for (int i = 0; i < sz; i++)
//		printf("%d ", arr[i]);
//	printf("\n");
//	return 0;
//}