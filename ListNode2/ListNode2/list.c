#define _CRT_SECURE_NO_WARNINGS

#include "list.h"

ListNode* ListCreate()
{
	ListNode* phead = (ListNode*)malloc(sizeof(ListNode));
	if (phead == NULL)
	{
		perror("malloc fail");
	}
	assert(phead);
	phead->_data = -1;
	phead->_next = phead;
	phead->_prev = phead;
	return phead;
}

void ListDestory(ListNode* pHead)
{
	assert(pHead);

	ListNode* cur = pHead->_next;
	while (cur != pHead)
	{
		ListNode* next = cur->_next;
		free(cur);
		cur = next;
	}
}

void ListPrint(ListNode* pHead)
{
	assert(pHead);
	ListNode* cur = pHead->_next;
	printf("guard<==>");
	while (cur != pHead)
	{
		printf("%d<==>", cur->_data);
		cur = cur->_next;
	}
	printf("\n");
}

ListNode* BuyListnode(LTDataType x)
{
	ListNode* phead = (ListNode*)malloc(sizeof(ListNode));
	if (phead == NULL)
	{
		perror("malloc fail");
	}
	assert(phead);
	phead->_data = x;
	phead->_next = NULL;
	phead->_prev = NULL;
	return phead;
}

void ListPushBack(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* newnode = BuyListnode(x);
	ListNode* prevnode = pHead->_prev;

	prevnode->_next = newnode;
	newnode->_prev = prevnode;

	newnode->_next = pHead;
	pHead->_prev = newnode;
}

bool LTempty(ListNode* pHead)
{
	assert(pHead);

	return  pHead->_next == pHead;
}

void ListPopBack(ListNode* pHead)
{
	assert(pHead);
	assert(!LTempty(pHead));

	ListNode* tail = pHead->_prev;

	pHead->_prev = tail->_prev;
	tail->_prev->_next = pHead;

	free(tail);
	tail = NULL;
}

void ListPushFront(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* next = pHead->_next;
	ListNode* newnode = BuyListnode(x);
	assert(newnode);

	pHead->_next = newnode;
	newnode->_prev = pHead;

	next->_prev = newnode;
	newnode->_next = next;
}

void ListPopFront(ListNode* pHead)
{
	assert(pHead);
	assert(!LTempty(pHead));
	ListNode* first = pHead->_next;
	ListNode* second = first->_next;

	pHead->_next = second;
	second->_prev = pHead;

	free(first);
	first = NULL;
}

ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* cur = pHead->_next;

	while (cur->_data != x)
	{
		cur = cur->_next;
	}

	return cur;
}

void ListInsert(ListNode* pos, LTDataType x)
{
	ListNode* posprev = pos->_prev;
	ListNode* newnode = BuyListnode(x);
	assert(newnode);
	assert(pos);

	posprev->_next = newnode;
	newnode->_prev = posprev;

	newnode->_next = pos;
	pos->_prev = newnode;
}

void ListErase(ListNode* pos)
{
	assert(pos);
	ListNode* posprev = pos->_prev;
	ListNode* posnext = pos->_next;

	posprev->_next = posnext;
	posnext->_prev = posprev;

	free(pos);
	pos = NULL;
}

