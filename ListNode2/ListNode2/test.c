#define _CRT_SECURE_NO_WARNINGS

#include "list.h"

void test1()
{
	ListNode* plist = ListCreate();

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);

	ListPrint(plist);

	ListPopBack(plist);
	ListPrint(plist);

	ListPushFront(plist,10);
	ListPrint(plist);

	ListPushFront(plist, 20);
	ListPrint(plist);

	ListPopFront(plist);
	ListPrint(plist);

	ListNode* pos = ListFind(plist, 2);
	ListInsert(pos, 30);
	ListPrint(plist);

	ListErase(pos);
	ListPrint(plist);

	ListDestory(plist);
	plist = NULL;
}

int main()
{
	test1();
	return 0;
}