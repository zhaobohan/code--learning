#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int LTDataType;
typedef struct ListNode
{
	LTDataType data;
	struct ListNode* next;
}ListNode;

void ListNodePush(ListNode** phead,LTDataType x)
{
	assert(phead);
	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return;
	}
	newnode->data = x;
	newnode->next = NULL;

	if (*phead == NULL)
	{
		*phead = newnode;
		newnode->next = newnode;
	}
	else
	{
		ListNode* tail = *phead;
		while (tail->next != *phead)
		{
			tail = tail->next;
		}
		newnode->next = *phead;
		*phead = newnode;
		tail->next = *phead;
	}
}

void ListNodePop(ListNode** phead,ListNode* cur)
{
	ListNode* prev = *phead;
	ListNode* next = *phead;
	while (prev->next != cur)
	{
		prev = prev->next;
	}
	next = cur->next;
	prev->next = next;
	//free(cur);
	//cur = NULL;
}

void ListNodePrint(ListNode* head)
{
	assert(head);
	ListNode* cur = head->next;
	printf("%d->", head->data);
	while (cur!=head)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
}

int main()
{
	ListNode* plist = NULL;
	for (int i = 30; i > 0; i--)
	{
		ListNodePush(&plist, i);
	}
	int a = 15;
	ListNode* cur = plist;
	while (a--)
	{
		for (int i = 0; i < 8; i++)
		{
			cur = cur->next;
		}
		ListNodePop(&plist, cur);
		cur = cur->next;
	}
	ListNodePrint(plist);
	return 0;
}