#define _CRT_SECURE_NO_WARNINGS

#include "Heap.h"

void HeapSort(HP* php, int n)
{
	for (int i = 1; i < n; i++)
	{
		//������
		AdjustUp(php, i);
	}

	int end = n - 1;
	while (end > 0)
	{
		Swap(&php->a[0], &php->a[end]);
		AdjustDown(php, end, 0);
		end--;
	}
}

int main()
{
	HP hp;
	HeapInit(&hp);
	int arr[] = { 9,8,6,5,43,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < sz; i++)
	{
		HeapPush(&hp, arr[i]);
	}
	//HeapPop(&hp);
	HeapSort(&hp, sz);
	return 0;
}