#define _CRT_SECURE_NO_WARNINGS

#include "sort.h"

void PrintArrey(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void TestHeapSort()
{
	int a[] = { 9,8,7,6,5,1,2,3,4 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintArrey(a, n);
	HeapSort(a, n);
	PrintArrey(a, n);
}

void TestInsertSort()
{
	int a[] = { 9,8,7,6,5,1,2,3,4 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintArrey(a, n);
	InsertSort(a, n);
	PrintArrey(a, n);
}

void TestShellSort()
{
	int a[] = { 9,8,7,6,5,1,2,3,4 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintArrey(a, n);
	ShellSort(a, n);
	PrintArrey(a, n);
}

void TestBubbleSort()
{
	int a[] = { 9,8,7,6,5,1,2,3,4 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintArrey(a, n);
	BubbleSort(a, n);
	PrintArrey(a, n);
}

void TestMergeSort()
{
	int a[] = { 9,8,7,6,5,1,2,3,4 };
	int n = sizeof(a) / sizeof(a[0]);
	PrintArrey(a, n);
	MergeSort(a, n);
	PrintArrey(a, n);
}

int main()
{
	//TestHeapSort();
	//TestInsertSort();
	//TestShellSort();
	//TestBubbleSort();
	//TestMergeSort();
	printf("%d", sizeof(long));
	return 0;
}