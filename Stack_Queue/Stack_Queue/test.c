#define _CRT_SECURE_NO_WARNINGS
#include "stack.h"
#include "queue.h"

// 队列模拟实现栈

//typedef struct Mystack
//{
//	Queue push;
//	Queue pop;
//}Mystack;
//
//void MsInit(Mystack* ps)
//{
//	assert(ps);
//	QueueInit(&(ps->push));
//	QueueInit(&(ps->pop));
//}
//
//void MsPush(Mystack* ps,QDataType x)
//{
//	assert(ps);
//	QueuePush(&(ps->push), x);
//}
//
//void MsPop(Mystack* ps)
//{
//	while (QueueSize(&(ps->push)) > 1)
//	{
//		QueuePush(&(ps->pop), QueueFront(&(ps->push)));
//		QueuePop(&(ps->push));
//	}
//	QueuePop(&(ps->push));
//	while (!QueueEmpty(&(ps->pop)))
//	{
//		QueuePush(&(ps->push), QueueFront(&(ps->pop)));
//		QueuePop(&(ps->pop));
//	}
//}
//
//QDataType MsTop(Mystack* ps)
//{
//	assert(ps);
//	return ps->push.ptail->data;
//}
//
//bool MsEmpty(Mystack* ps)
//{
//	if (ps->push.size == 0)
//		return true;
//	return false;
//}
//
//int main()
//{
//	Mystack s;
//	MsInit(&s);
//	MsPush(&s, 1);
//	MsPush(&s, 2);
//	MsPush(&s, 3);
//	MsPush(&s, 4);
//	MsPush(&s, 5);
//	while (!MsEmpty(&s))
//	{
//		printf("%d ", MsTop(&s));
//		MsPop(&s);
//	}
//	return 0;
//}

// 栈模拟实现队列

typedef struct Myqueue
{
	Stack Push;
	Stack Pop;
}Myqueue;

void MqInit(Myqueue* pq)
{
	assert(pq);
	StackInit(&(pq->Push));
	StackInit(&(pq->Pop));
}

void MqPush(Myqueue* pq,STDataType x)
{
	assert(pq);
	StackPush(&(pq->Push), x);
}

void MqPop(Myqueue* pq)
{
	while (!StackEmpty(&(pq->Push)))
	{
		StackPush(&(pq->Pop), StackTop(&(pq->Push)));
		StackPop(&(pq->Push));
	}
	StackPop(&(pq->Pop));
	while (!StackEmpty(&(pq->Pop)))
	{
		StackPush(&(pq->Push), StackTop(&(pq->Pop)));
		StackPop(&(pq->Pop));
	}
}

STDataType MqTop(Myqueue* pq)
{
	// 把数据从push弄到pop
	while (!StackEmpty(&(pq->Push)))
	{
		StackPush(&(pq->Pop), StackTop(&(pq->Push)));
		StackPop(&(pq->Push));
	}
	STDataType ret = pq->Pop._a[pq->Pop._top-1];
	// 再把数据弄回去
	while (!StackEmpty(&(pq->Pop)))
	{
		StackPush(&(pq->Push), StackTop(&(pq->Pop)));
		StackPop(&(pq->Pop));
	}
	return ret;
}

int MqEmpty(Myqueue* pq)
{
	if (pq->Push._top == 0)
		return 1;
	return 0;
}

int main()
{
	Myqueue q;
	MqInit(&q);
	MqPush(&q, 1);
	MqPush(&q, 2);
	MqPush(&q, 3);
	MqPush(&q, 4);
	MqPush(&q, 5);
	while (!MqEmpty(&q))
	{
		printf("%d ", MqTop(&q));
		MqPop(&q);
	}
	return 0;
}