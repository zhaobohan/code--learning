#define _CRT_SECURE_NO_WARNINGS
#include "game.h"
void menu()
{
	printf("*********************\n");
	printf("****** 1.play *******\n");
	printf("****** 0.exit *******\n");
	printf("*********************\n");
}

void Initboard(char board[ROW][CAL], int row, int cal)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < cal; j++)
		{
			board[i][j] = ' ';
		}
	}
}

void Displayboard(char board[ROW][CAL], int row, int cal)
{
	for (int i = 0; i < row; i++)
	{
		//打印横行数字
		for (int j = 0; j < cal; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < cal - 1)
				printf("|");
			if (j == cal-1)
				printf("\n");
		}

		//打印分隔符
		if (i < row - 1)
		{
			for (int j = 0; j < row; j++)
			{
				printf("---");
				if (j < row - 1)
					printf("-");
				if (j == row - 1)
					printf("\n");
			}
		}

	}
}

void Playermove(char board[ROW][CAL], int row, int cal)
{
	int x, y;
	printf("It is your turn:->");
	scanf("%d %d", &x, &y);
	if (x >= 1 && x <= 3 && y >= 1 && y <= 3)
	{
		if (board[x - 1][y - 1] == ' ')
			board[x - 1][y - 1] = '*';
		else
			printf("Choice another place!\n");
	}
	else
		printf("Error!Try again!\n");
	Displayboard(board, ROW, CAL);
}

void Computermove(char board[ROW][CAL], int row, int cal)
{
	srand((unsigned int)time(NULL));
	//Sleep(1000);
	printf("Computer move...\n");
	//Sleep(3600);
	int x, y;
	while (1)
	{
		x = rand() % ROW;  //0 1 2
		y = rand() % CAL; 
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
	Displayboard(board, ROW, CAL);
}

int IsFull(char board[ROW][CAL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
			{
				return 0;
			}
		}
	}
	return 1;
}

char IsWin(char board[ROW][CAL], int row, int col)
{
	//行
	int i = 0;
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
		{
			return board[i][0];
		}
	}
	//列
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')
		{
			return board[0][i];
		}
	}
	//对角线
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
		return board[1][1];
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
		return board[1][1];

	//平局
	if (IsFull(board, row, col) == 1)
	{
		return 'Q';
	}
	//继续
	return 'C';
}

