#pragma once
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>

#define ROW 3
#define CAL 3

void menu();

void Initboard(char board[ROW][CAL], int row, int cal);

void Displayboard(char board[ROW][CAL], int row, int cal);

void Playermove(char board[ROW][CAL], int row, int cal);

void Computermove(char board[ROW][CAL], int row, int cal);

int IsFull(char board[ROW][CAL], int row, int col);

char IsWin(char board[ROW][CAL], int row, int col);





