#define _CRT_SECURE_NO_WARNINGS

#include "game.h"

void game()
{
	char board[ROW][CAL];
	//初始化
	Initboard(board, ROW, CAL);
	//打印棋盘
	Displayboard(board, ROW, CAL);
	char ret = 0;
	while (1)
	{
		Playermove(board, ROW, CAL);
		ret = IsWin(board, ROW, CAL);
		if (ret != 'C')
			break;
		Computermove(board, ROW, CAL);
		ret = IsWin(board, ROW, CAL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
		printf("玩家赢\n");
	else if (ret == '#')
		printf("电脑赢\n");
	else
		printf("平局\n");
}

int main()
{
	int input = 0;
	do
	{
		menu();
		printf("Make your choice:->");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			break;
		default:
			break;
		}

	} while (input);
	return 0;
}