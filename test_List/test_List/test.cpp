#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef struct ListNode
{
	int val;
	struct ListNode* next;
}ListNode;



//struct ListNode* Reverse(struct ListNode* head)
//{
//	struct ListNode* cur = head;
//	struct ListNode* prev = NULL;
//	while (cur)
//	{
//		struct ListNode* next = cur->next;
//		cur->next = prev;
//		prev = cur;
//		cur = next;
//		if (next)
//			next = next->next;
//	}
//	return prev;
//}
//
//bool isPalindrome(struct ListNode* head)
//{
//	struct ListNode* reversehead = Reverse(head);
//	struct ListNode* cur1 = head;
//	struct ListNode* cur2 = reversehead;
//	while (cur1)
//	{
//		if (cur1->val != reversehead->val)
//		{
//			return false;
//		}
//		cur1 = cur1->next;
//		reversehead = reversehead->next;
//	}
//	return true;
//}
//
//
//struct ListNode* reverseList(struct ListNode* head)
//{
//	if (head == NULL)
//	{
//		return NULL;
//	}
//	struct ListNode* newhead = NULL;
//	struct ListNode* cur = head;
//	while (cur)
//	{
//		struct ListNode* next = cur->next;
//		cur->next = newhead;
//		newhead = cur;
//		cur = next;
//	}
//	return newhead;
//}
//
//
//bool chkPalindrome(ListNode* A)
//{
//	// write code here
//	struct ListNode* newhead = reverseList(A);
//	struct ListNode* cur1 = A;
//	struct ListNode* cur2 = newhead;
//	while (cur1)
//	{
//		if (cur1->val != cur2->val)
//		{
//			return false;
//		}
//		cur1 = cur1->next;
//		cur2 = cur2->next;
//	}	
//	return true;
//}

//struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2)
//{
//    struct ListNode* head = NULL;
//    struct ListNode* tail = NULL;
//    int cut = 0;
//
//    while (l1 || l2)
//    {
//        int n1 = l1 ? l1->val : 0;
//        int n2 = l2 ? l2->val : 0;
//        cut = 0;
//        int sum = n1 + n2 + cut;
//        if (head == NULL)
//        {
//            head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
//            tail->val = sum % 10;
//            tail->next = NULL;
//        }
//        else
//        {
//            tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//            tail = tail->next;
//            tail->val = sum % 10;
//            tail->next = NULL;
//        }
//        cut = sum / 10;
//    }
//    if (cut > 0)
//    {
//        tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//        tail = tail->next;
//        tail->val = cut;
//        tail->next = NULL;
//    }
//    return head;
//}

//ListNode* reverseList(ListNode* head)
//{
//    struct ListNode* cur = head;
//    struct ListNode* prev = nullptr;
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//        cur->next = prev;
//        prev = cur;
//        cur = next;
//    }
//    return prev;
//}
//
//ListNode* Getmid(ListNode* head)
//{
//    ListNode* fast = head;
//    ListNode* slow = head;
//    while (fast && fast->next)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//    }
//    return slow;
//}
//
//bool isPalindrome(ListNode* head)
//{
//    ListNode* mid = Getmid(head);
//    ListNode* reverse = reverseList(mid);
//    while (reverse)
//    {
//        if (reverse->val != head->val)
//        {
//            return false;
//        }
//        reverse = reverse->next;
//        head = head->next;
//    }
//    return true;
//}

void deleteNode(struct ListNode* node)
{
	struct ListNode* cur = node;
	struct ListNode* prev = NULL;
	while (cur)
	{
		struct ListNode* next = cur->next;
		if (next == NULL)
		{
			prev->next = NULL;
		}
		else
		{
			cur->val = next->val;
			prev = cur;
			cur = next;
			next = next->next;
		}
	}
}

int main()
{
	struct ListNode* n1 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n1);
	struct ListNode* n2 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n2);
	struct ListNode* n3 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n3);
	struct ListNode* n4 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n4);
	struct ListNode* n5 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n5);
	struct ListNode* n6 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n6);
	struct ListNode* n7 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n7);
	struct ListNode* n8 = (struct ListNode*)malloc(sizeof(struct ListNode));
	assert(n8);

	n1->val = 1;
	n2->val = 2;
	n3->val = 3;

	n4->val = 2;
	n5->val = 1;
	n6->val = 4;

	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = n5;
	n5->next = nullptr;
	n6->next = NULL;


	deleteNode(n2);
	return 0;
}
