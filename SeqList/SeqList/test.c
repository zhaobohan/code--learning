#define _CRT_SECURE_NO_WARNINGS

#include "SeqList.h"

void test1()
{
	SeqList ps;
	SeqListInit(&ps);
 	SeqListPushBack(&ps, 1);
	SeqListPushBack(&ps, 2);
	SeqListPushBack(&ps, 3);
	SeqListPushBack(&ps, 4);
	SeqListPushBack(&ps, 5);
	SeqListPrint(&ps);
	SeqListPushFront(&ps, 9);
	SeqListPushFront(&ps, 8);
	SeqListPushFront(&ps, 7);
	SeqListPrint(&ps);
	SeqListPopFront(&ps);
	SeqListPopBack(&ps);
	SeqListPrint(&ps);
	SeqListDestroy(&ps);
}

int main()
{
	test1();
	return 0;
}