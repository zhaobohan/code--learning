#define _CRT_SECURE_NO_WARNINGS

//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC

#include <stdio.h>
#include <string.h>

int Reverse(char s1[], char s2[],int len)
{
	for (int i = 0; i < len - 1; i++)  //最多反转len-1次
	{
		char tmp = *(s1);
		for (int j = 0; j < len-1; j++)
		{
			s1[j] = s1[j+1];
		}
		s1[len - 1] = tmp;
		//完成了一次翻转
		if (strcmp(s1, s2) == 0)
		{
			return 1;//相等了
		}
	}
	return 0;
}

int main()
{
	char s1[1000], s2[1000];
	scanf("%s %s", s1, s2);
	int len = strlen(s1);
	int ret=Reverse(s1, s2,len);
	printf("%d\n", ret);
	return 0;
}




//#include <stdio.h>
//
//int find(int a[3][3], int row, int col, int key)
//{
//	int i = 0;
//	int j = col - 1;
//	while (i < row && j >= 0)
//	{
//		if (key > a[i][j])
//		{
//			i++;
//		}
//		else if (key < a[i][j])
//		{
//			j--;
//		}
//		else
//		{
//			return 1;
//		}
//	}
//}
//
//int main()
//{
//	int arr[3][3] = { 1, 3, 5 , 3, 5, 7 , 5, 7, 9 };
//	int finder = 0;
//	scanf("%d", &finder);
//	int res = find(arr, 3, 3, finder);
//	if (res == 1)
//	{
//		printf("找到了\n");
//	}
//	else 
//	{
//		printf("没找到\n");
//	}
//	return 0;
//}



//#include <stdio.h>
//
//void menu()
//{
//	printf("0.exit 1.add 2.sub 3.mul 4.div\n");
//}
//
//int add(int x, int y)
//{
//	return x + y;
//}
//
//int sub(int x, int y)
//{
//	return x - y;
//}
//
//int mul(int x, int y)
//{
//	return x * y;
//}
//
//int div(int x, int y)
//{
//	return x / y;
//}
//
//int main()
//{
//	int input = 0;
//	int a, b;
//	int ret;
//	int (*p[5])(int x,int y) = { 0,add,sub,mul,div };//借助函数指针数组把这些函数管理了起来
//	do
//	{
//		menu();
//		scanf("%d", &input);
//		printf("输入两个数");
//		scanf("%d %d", &a, &b);
//		ret = (* p[input])(a, b);
//		printf("%d\n", ret);
//	} while (input);
//	return 0;
//}



//#include <stdio.h>
//
//void print(int(*p)[5], int row, int col)
//{
//	for (int i = 0; i < row; i++)
//	{
//		for (int j = 0; j < col; j++)
//		{
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { 1,2,3,4,5,6 };
//	print(arr, 3, 5);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = &arr;
//}