#define _CRT_SECURE_NO_WARNINGS

#include "SList.h"

void test1()
{
	SListNode* plist = NULL;
	printf("创建链表\n");
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);
	SListPrint(plist);

	printf("头插\n");
	SListPushFront(&plist, 10);
	SListPrint(plist);

	printf("尾删\n");
	SListPopBack(&plist);
	SListPrint(plist);

	printf("头删\n");
	SListPopFront(&plist);
	SListPrint(plist);

	printf("3后插\n");
	SListNode* pos = SListFind(plist, 3);
	SListInsertAfter(pos, 40);
	SListPrint(plist);

	printf("3后删\n");
	SListEraseAfter(pos);
	SListPrint(plist);


	printf("3后删\n");
	SListEraseAfter(pos);
	SListPrint(plist);
}

int main()
{
	test1();
	return 0;
}