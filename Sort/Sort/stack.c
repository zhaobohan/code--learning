#define _CRT_SECURE_NO_WARNINGS
#include "stack.h"

void StackInit(Stack* ps)
{
	assert(ps);
	ps->_a = NULL;
	ps->_top = 0;
	ps->_capacity = 0;
}

void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	if (ps->_capacity == ps->_top)
	{
		STDataType* tmp = NULL;
		int newcapacity = ps->_capacity == 0 ? 4:ps->_capacity * 2;
		tmp = (STDataType*)realloc(ps->_a,sizeof(STDataType)* newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		ps->_capacity = newcapacity;
		ps->_a = tmp;
	}
	ps->_a[ps->_top] = data;
	ps->_top++;
}

bool STEmpty(Stack* ps)
{
	assert(ps);
	
	return ps->_top == 0;
}

void StackPop(Stack* ps)
{
	assert(ps);
	assert(!STEmpty(ps));

	ps->_top--;
}

STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(!STEmpty(ps));
	
	return ps->_a[ps->_top-1];
}

int StackSize(Stack* ps)
{
	assert(ps);
	return ps->_top;
}

int StackEmpty(Stack* ps)
{
	assert(ps);
	if (0 == ps->_top)
		return 1;
	else
		return 0;
}

void StackDestroy(Stack* ps)
{
	assert(ps);
	ps->_capacity = 0;
	ps->_top = 0;
	free(ps->_a);
	ps->_a = NULL;
}