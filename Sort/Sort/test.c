#define _CRT_SECURE_NO_WARNINGS
#include "sort.h"

void TestInsertSort()
{
	printf("InsetSort\n");
	int arr[] = { 9,6,4,7,2,3,11 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	InsertSort(arr, sz);
	PrintArrey(arr, sz);
}

void TestBubbleSort()
{
	printf("BubbleSort\n");
	int arr[] = { 9,6,4,7,2,3,11 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	BubbleSort(arr, sz);
	PrintArrey(arr, sz);
}

void TestHeapSort()
{
	printf("HeapSort\n");
	int arr[] = { 9,6,4,7,2,3,11 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	HeapSort(arr, sz);
	PrintArrey(arr, sz);
}

void TestShellSort()
{
	printf("ShellSort\n");
	int arr[] = { 9,6,4,7,2,3,11 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	ShellSort(arr, sz);
	PrintArrey(arr, sz);
}

void TestSelectSort()
{
	printf("SelectSort\n");
	int arr[] = { 9,6,4,7,2,3,11 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	SelectSort(arr, sz);
	PrintArrey(arr, sz);
}

void TestQuickSort()
{
	printf("QuickSort\n");
	int arr[] = { 6,1,2,7,9,3,4,5,10,8 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	QuickSort(arr, 0, sz - 1);
	PrintArrey(arr, sz);
}

void TestMergeSort()
{
	printf("MergeSort\n");
	int arr[] = { 6,1,2,7,9,3,4,5,10,8 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	PrintArrey(arr, sz);
	MergeSort(arr, sz);
	PrintArrey(arr, sz);
}

int main()
{
	//TestInsertSort();
	//TestBubbleSort();
	//TestHeapSort();
	//TestShellSort();
	//TestSelectSort();
	//TestQuickSort();
	TestMergeSort();
}