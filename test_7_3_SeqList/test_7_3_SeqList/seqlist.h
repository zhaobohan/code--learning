#pragma once
#include <stdio.h>
#include <stdlib.h>

typedef int SLDataType;

typedef struct SeqList
{
	SLDataType* p;
	int sz;
	int capcity;
}SeqList;



//˳�����ʼ��
void SeqListInit(SeqList* psl);

// ���ռ䣬������ˣ���������
void CheckCapacity(SeqList* psl);

// ˳���β��
void SeqListPushBack(SeqList* psl, SLDataType x);

// ˳���βɾ
void SeqListPopBack(SeqList* psl);

// ˳���ͷ��
void SeqListPushFront(SeqList* psl, SLDataType x);

// ˳���ͷɾ
void SeqListPopFront(SeqList* psl);

// ˳�������
void SeqListDestory(SeqList* psl);

// ˳�����ӡ
void SeqListPrint(SeqList* psl);
