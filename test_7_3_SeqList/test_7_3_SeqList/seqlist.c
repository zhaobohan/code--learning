#define _CRT_SECURE_NO_WARNINGS

#include "seqlist.h"

void SeqListInit(SeqList* psl)
{
	psl->p = (SLDataType*)malloc(sizeof(SLDataType) * 4);
	if (psl->p == NULL)
	{
		perror("malloc fail");
		return;
	}
	psl->sz = 0;
	psl->capcity = 4;
}

void CheckCapacity(SeqList* psl)
{
	if (psl->sz == psl->capcity)
	{
		SLDataType* tmp = (SLDataType*)realloc(psl->p, sizeof(SLDataType)*psl->capcity*2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		psl->p = tmp;
		psl->capcity *= 2;
		printf("增容成功，当前容量%d\n", psl->capcity);
	}
}

void SeqListPushBack(SeqList* psl, SLDataType x)
{
	CheckCapacity(psl);
	psl->p[psl->sz++] = x;
}

//void SLPushBack(SL* psl, SLDatatype x)
//{
//	//psl->a[psl->size] = x;
//	//psl->size++;
//
//	SLCheckCapacity(psl);
//
//	psl->a[psl->size++] = x;
//}

void SeqListPopBack(SeqList* psl)
{
	(psl->sz)--;
}

void SeqListDestory(SeqList* psl)
{
	free(psl->p);
	psl->p = NULL;
	psl->sz = 0;
	psl->capcity = 0;
}

void SeqListPrint(SeqList* psl)
{
	for (int i = 0; i < psl->sz; i++)
	{
		printf("%d ", psl->p[i]);
	}
}

void SeqListPushFront(SeqList* psl, SLDataType x)
{
	CheckCapacity(psl);
	int end = psl->sz - 1;
	while (end >= 0)
	{
		psl->p[end+1] = psl->p[end];
		end--;
	}
	psl->p[0] = x;
	psl->sz++;
}

void SeqListPopFront(SeqList* psl)
{
	int begin = 0;
	while (begin < psl->sz)
	{
		psl->p[begin] = psl->p[begin + 1];
		begin++;
	}
	psl->sz--;
}