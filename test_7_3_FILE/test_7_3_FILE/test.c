#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void rotate(int* nums, int numsSize, int k) //3
{
    int j = k & numsSize;
    printf("[");
    for (int i = j; i < numsSize-1; i++)
    {
        printf("%d,",nums[i + 1]);
    }
    for (int i = 0; i <= j; i++)
    {
        if (i < j)
        {
            printf("%d,", nums[i]);
        }
        else
        {
            printf("%d", nums[i]);
        }
    }
    printf("]");
}

int main()
{
    int arr[] = { 1,2,3,4,5,6,7 };
    int k = 3;
    int sz = 7;
    rotate(arr, sz, k);
    return 0;
}


//#include <stdio.h>
//int main()
//{
//	FILE* pf;
//	//打开文件
//	pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen;");
//		return 1;
//	}
//	//操作文件
//	char ch[] = "abcdef";
//	fputs(ch, pf);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	FILE* pf2;
//	pf2 = fopen("test.txt", "r");
//	if (pf2 == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//操作文件
//	printf("%c\n", fgetc(pf2));//读取a
//	printf("%c\n", fgetc(pf2));//读取b，此时指向c
//	fseek(pf2, -1, SEEK_CUR);//把文件指针从当前位置向左移动一位
//	printf("%c\n", fgetc(pf2));//此时文件指向b，输出就是b
//	fseek(pf2, 0, SEEK_SET);//把文件指针指向开始
//	printf("%c\n", fgetc(pf2));
//	//关闭文件
//	fclose(pf2);
//	pf2 = NULL;
//	return 0;
//}

//int main()
//{
//	FILE* pf;
//	打开文件
//	pf = fopen("text1.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	操作文件
//	char str[20];
//	fgets(str, 20, pf);
//	printf("%s", str);
//	fclose(pf);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	FILE* pf;
//	//打开文件
//	pf = fopen("text1.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//操作文件
//	char arr[] = "helloworld";
//	fputs(arr, pf);
//	fclose(pf);
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//对文件进行操作
//	fputc('a', pf);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//int main()
//{
//	//文件的打开
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen\n");
//		return 1;
//	}
//	//进行文件的相关操作
//	fputs("打开文件成功!\n",pf);
//	//关闭文件
//	fclose(pf);
//	return 0;
//}