#define _CRT_SECURE_NO_WARNINGS

//strstr模拟实现
//#include <stdio.h>

//char* my_strstr(const char* str1, const char* str2)
//{
//	char* s1 = (char*)str1;
//	char* s2 = (char*)str2;
//	char* cp = (char*)str2;
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = (char*)str2;
//		while (*s1 && *s2 && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return cp;
//		}
//	}
//	return NULL;
//}
//
//char* my_strstr(const char* str1, const char* str2)
//{
//	char* s1 = (char*)str1;
//	char* s2 = (char*)str2;
//	char* cp = (char*)str2;
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = (char*)str2;
//		while (*s1 && *s2 && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return cp;
//		}
//	}
//	return NULL;
//}
//
//
//
//int main()
//{
//	char str1[] = { "abcde" };
//	char str2[] = { "bcd" };
//	printf("%s\n", my_strstr(str1, str2));
//	return 0;
//}
//


//strcmp函数模拟
#include <stdio.h>
#include <string.h>

int my_strcmp(char* arr1, char* arr2)
{
	int ret = 0;
	while (*arr1 != '\0' && *arr2 != '\0')
	{
		if (*arr1 > *arr2)
		{
			ret = 1;
			break;
		}
		else if (*arr1 < *arr2)
		{
			ret = -1;
			break;
		}
		else
		{
			arr1++;
			arr2++;
		}
	}
	return ret;
}

int main()
{
	char arr1[] = { "abcde" };
	char arr2[] = { "abcde\0fg" };
	int len = my_strcmp(arr1, arr2);
	printf("%d", len);
	return 0;
}


////strcat函数模拟
//#include <stdio.h>
//#include <string.h>
//
//char* my_strcat(char* arr1, char* arr2)
//{
//	char* ret = arr1;
//	while (*arr1 != '\0')
//	{
//		arr1++;
//	}
//	while (*arr2 != '\0')
//	{
//		*arr1 = *arr2;
//		arr2++;
//		arr1++;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "abcdef";
//	char arr2[] = "ghi";
//	printf("%s\n", my_strcat(arr1, arr2));
//	return 0;
//}

////strcpy函数模拟
//#include <stdio.h>
//
//void my_strcpy1(char* arr2, const char* arr1)
//{
//	do
//	{
//		*arr2 = *arr1;
//		arr2++;
//		arr1++;
//	} while (*arr1 != '\0');
//}
//
//char* my_strcpy2(char* str2, const char* str1)
//{
//	char* ret = str1;
//	while (*str2++ = *str1++);
//	return ret;
//}
//
//int main()
//{
//	char arr1[] = { "abcdef" };
//	char arr2[10] = { 0 };
//	char arr3[10] = { 0 };
//	my_strcpy1(arr2, arr1);
//	my_strcpy2(arr3, arr1);
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	printf("%s\n", arr3);
//	printf("%s\n", my_strcpy2(arr3, arr1));
//	return 0;
//}



////strlen函数模拟
//#include <stdio.h>
//#include <string.h>
//
////计数器
//size_t my_strlen1(const char* arrr)
//{
//	int ret = 0;
//	while (*arrr != '\0')
//	{
//		ret++;
//		arrr++;
//	}
//	return ret;
//}
//
////指针-指针
//size_t my_strlen2(const char* arr)
//{
//	int ret = 0;
//	const char* p = arr;
//	while (*arr != '\0')
//	{
//		arr++;
//	}
//	ret =(int)(arr - p);
//	return ret;
//}
//
////递归
//size_t my_strlen3(const char* str)
//{
//	if (*str == '\0')
//	{
//		return 0;
//	}
//	else
//	{
//		return 1 + my_strlen3(str + 1);
//	}
//}
//
//int main()
//{
//	char arr[] = "abcd";
//	int len1 = (int) my_strlen1(arr);
//	int len2 = (int)my_strlen2(arr);
//	int len3 = (int)my_strlen3(arr);
//	printf("%d\n", len1);
//	printf("%d\n", len2);
//	printf("%d\n", len3);
//	return 0;
//}



