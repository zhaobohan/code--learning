#define _CRT_SECURE_NO_WARNINGS

//建立有序表：12，23，46，67，85
//建立有序表：5，59，94
//两个有序顺序表合并为一个有序顺序表，验证代码的正确性。

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLDataType;
typedef struct SeqList
{
	SLDataType* a;
	int size;
	int capacity;
}SeqList;

void SListInit(SeqList* ps)
{
	//assert(ps);
	ps->size = 0;
	ps->capacity = 4;
	ps->a = (SLDataType*)malloc(sizeof(SLDataType) * 4);
	if (ps->a == NULL)
	{
		perror("malloc fail");
		return;
	}
}

void SListDestroy(SeqList* ps)
{
	assert(ps);
	ps->a = NULL;
	ps->capacity = 0;
	ps->size = 0;
}

void SListCheckCapacity(SeqList* ps)
{
	assert(ps);
	if (ps->size == ps->capacity)
	{
		SLDataType* tmp = NULL;
		tmp = (SLDataType*)realloc(ps->a, sizeof(SLDataType)*(ps->capacity) * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		ps->a = tmp;
		ps->capacity *= 2;
		//printf("The capacity now:%d\n", ps->capacity);
	}
	else
	{
		return;
	}
}

void SListPushBack(SeqList* ps, SLDataType x)
{
	assert(ps);
	SListCheckCapacity(ps);
	ps->a[ps->size] = x;
	ps->size++;
}

void SListPrint(SeqList* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SListMerge(SeqList* ps1, SeqList* ps2)
{
	assert(ps1);
	assert(ps2);
	SeqList ps;
	SListInit(&ps);
	for (int i = 0; i < ps1->size; i++)
	{
		SListPushBack(&ps, ps1->a[i]);
	}
	for (int i = 0; i < ps2->size; i++)
	{
		SListPushBack(&ps, ps2->a[i]);
	}
	printf("The result of merging two seqlists is:\n");
	SListPrint(&ps);
}

int main()
{
	SeqList ps1;
	SeqList ps2;
	SListInit(&ps1);
	SListInit(&ps2);
	int quantity1 = 0, quantity2 = 0, input = 0;

	printf("Input quantity of seqlist1-> ");
	scanf("%d", &quantity1);
	for (int i = 0; i < quantity1; i++)
	{
		scanf("%d", &input);
		SListPushBack(&ps1, input);
	}

	printf("Input quantity of seqlist2-> ");
	scanf("%d", &quantity2);
	for (int i = 0; i < quantity2; i++)
	{
		scanf("%d", &input);
		SListPushBack(&ps2, input);
	}

	SListMerge(&ps1, &ps2);
	return 0;
}