#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
int main()
{
    union
    {
        short k;
        char i[2];
    }*s, a;
    s = &a;
    s->i[0] = 0x39;
    s->i[1] = 0x38;
    printf("%x\n", a.k);
    return 0;
}

//
//#include<stdio.h>
//#include<stdlib.h>
//#include<conio.h>
//#include<dos.h>
//#include<string.h>
//
//#define LEN sizeof(struct student)
//
//#define FORMAT "%-8d%-15s%-15s%-12.1lf%-12.1lf%-12.1lf%-12.1lf\n"
//
//#define DATA stu[i].num,stu[i].name,stu[i].sex,stu[i].elec,stu[i].expe,stu[i].requ,stu[i].sum
//
//struct student
//{
//    int num;
//    char name[15];
//    char sex[20];
//    double elec;	
//    double expe;	
//    double requ;	
//    double sum;		
//};
//
//struct student stu[50];	
//
//void menu()
//{
//    printf("\n");
//    printf("|---------------学生信息管理系统---------------|\n");
//    printf("1. 录入学生信息  ");
//    printf("2. 查找学生信息\n");
//    printf("3. 删除学生信息  ");
//    printf("4. 修改学生信息\n");
//    printf("5. 插入学生信息  ");
//    printf("6. 排序\n");
//    printf("7. 统计学生总数  ");
//    printf("8. 显示所有学生信息\n");
//    printf("0. 退出系统   \n");
//    printf("请选择(0-8):");
//}
//
//
//void in()
//{
//    int i, m = 0;
//    char ch[2];
//    FILE* fp;
//    if ((fp = fopen("data.txt", "a+")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))
//    {
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//        {
//            m++;
//        }
//    }
//    fclose(fp);
//    if (m == 0)
//    {
//        printf("文件中没有记录!\n");
//    }
//    if ((fp = fopen("data.txt", "wb")) == NULL)
//    {
//        printf("文件不存在！\n");
//    }
//    printf("输入学生信息(y/n):");
//    scanf("%s", ch);
//    while (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
//    {
//        printf("number:");
//        scanf("%d", &stu[m].num);
//        for (i = 0; i < m; i++)
//            if (stu[i].num == stu[m].num)
//            {
//                printf("number已经存在了，按任意键继续!");
//                fclose(fp);
//                return;
//            }
//        printf("name:");
//        scanf("%s", stu[m].name);
//        printf("sex:");
//        scanf("%s", stu[m].sex);
//        printf("elective:");
//        scanf("%lf", &stu[m].elec);
//        printf("experiment:");
//        scanf("%lf", &stu[m].expe);
//        printf("required course:");
//        scanf("%lf", &stu[m].requ);
//        stu[m].sum = stu[m].elec + stu[m].expe + stu[m].requ;
//        if (fwrite(&stu[m], LEN, 1, fp) != 1)
//        {
//            printf("不能保存!");
//        }
//        else
//        {
//            printf("%s 被保存!\n", stu[m].name);
//            m++;
//        }
//        printf("继续?(y/n):");
//        scanf("%s", ch);
//    }
//    fclose(fp);
//    printf("OK!\n");
//}
//
//void show()
//{
//    FILE* fp;
//    int i, m = 0;
//    fp = fopen("data.txt", "rb");
//    while (!feof(fp))
//    {
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;
//    }
//    fclose(fp);
//    printf("number  name           sex            elective    experiment  required    sum\t\n");
//    for (i = 0; i < m; i++)
//    {
//        printf(FORMAT, DATA);
//    }
//}
//
//void order()
//{
//    FILE* fp;
//    struct student t;
//    int i = 0, j = 0, m = 0;
//    if ((fp = fopen("data.txt", "r+")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;
//    fclose(fp);
//    if (m == 0)
//    {
//        printf("文件中没有记录!\n");
//        return;
//    }
//    if ((fp = fopen("data.txt", "wb")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    for (i = 0; i < m - 1; i++)
//        for (j = i + 1; j < m; j++)
//            if (stu[i].sum < stu[j].sum)
//            {
//                t = stu[i]; 
//                stu[i] = stu[j]; 
//                stu[j] = t;
//            }
//    if ((fp = fopen("data.txt", "wb")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    for (i = 0; i < m; i++)
//        if (fwrite(&stu[i], LEN, 1, fp) != 1)
//        {
//            printf("%s 不能保存文件!\n");
//        }
//    fclose(fp);
//    printf("保存成功\n");
//}
//
//void del()
//{
//    FILE* fp;
//    int snum, i, j, m = 0;
//    char ch[2];
//    if ((fp = fopen("data.txt", "r+")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))  
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;
//    fclose(fp);
//    if (m == 0)
//    {
//        printf("文件中没有记录！\n");
//    }
//    printf("请输入学生学号");
//    scanf("%d", &snum);
//    for (i = 0; i < m; i++)
//        if (snum == stu[i].num)
//        {
//            printf("找到了这条记录，是否删除?(y/n)");
//            scanf("%s", ch);
//            if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
//            {
//                for (j = i; j < m; j++)
//                    stu[j] = stu[j + 1];
//                m--;
//                if ((fp = fopen("data.txt", "wb")) == NULL)
//                {
//                    printf("文件不存在\n");
//                    return;
//                }
//                for (j = 0; j < m; j++)
//                    if (fwrite(&stu[j], LEN, 1, fp) != 1)
//                    {
//                        printf("can not save!\n");
//                    }
//                fclose(fp);
//                printf("删除成功!\n");
//            }
//            else {
//                printf("找到了记录，选择不删除！");
//            }
//            break;
//        }
//        else
//        {
//            printf("没有找到这名学生!\n");
//        }
//}
//
//void search()
//{
//    FILE* fp;
//    int snum, i, m = 0;
//    if ((fp = fopen("data.txt", "rb")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;
//    fclose(fp);
//    if (m == 0)
//    {
//        printf("文件中没有记录！\n");
//        return;
//    }
//    printf("请输入number:");
//    scanf("%d", &snum);
//    for (i = 0; i < m; i++)
//        if (snum == stu[i].num)
//        {
//            printf("number  name           sex            elective    experiment  required    sum\t\n");
//            printf(FORMAT, DATA);
//            break;
//        }
//    if (i == m) printf("没有找到这名学生!\n");
//}
//
//void modify()
//{
//    FILE* fp;
//    struct student t;
//    int i = 0, j = 0, m = 0, snum;
//    if ((fp = fopen("data.txt", "r+")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;
//    if (m == 0)
//    {
//        printf("文件中没有记录！\n");
//        fclose(fp);
//        return;
//    }
//    show();
//    printf("请输入要修改的学生number： ");
//    scanf("%d", &snum);
//    for (i = 0; i < m; i++)
//        if (snum == stu[i].num)
//        {
//            printf("找到了这名学生,可以修改他的信息!\n");
//            printf("name:");
//            scanf("%s", stu[i].name);
//            printf("sex:");
//            scanf("%s", stu[i].sex);
//            printf("elective:");
//            scanf("%lf", &stu[i].elec);
//            printf("experiment:");
//            scanf("%lf", &stu[i].expe);
//            printf("required course:");
//            scanf("%lf", &stu[i].requ);
//            printf("修改成功!");
//            stu[i].sum = stu[i].elec + stu[i].expe + stu[i].requ;
//
//            if ((fp = fopen("data.txt", "wb")) == NULL)
//            {
//                printf("can not open\n");
//                return;
//            }
//            for (j = 0; j < m; j++)
//                if (fwrite(&stu[j], LEN, 1, fp) != 1)
//                {
//                    printf("can not save!");
//                }
//            fclose(fp);
//            break;
//        }
//    if (i == m)
//        printf("没有找到这名学生!\n");
//}
//
//void insert()
//{
//    FILE* fp;
//    int i, j, k, m = 0, snum;
//    if ((fp = fopen("data.txt", "r+")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;
//    if (m == 0)
//    {
//        printf("文件中没有记录!\n");
//        fclose(fp);
//        return;
//    }
//    printf("请输入要插入的位置(number)：\n");
//    scanf("%d", &snum);
//    for (i = 0; i < m; i++)
//        if (snum == stu[i].num)
//            break;
//    for (j = m - 1; j > i; j--)
//        stu[j + 1] = stu[j];
//    printf("现在请输入要插入的学生信息.\n");
//    printf("number:");
//    scanf("%d", &stu[i + 1].num);
//    for (k = 0; k < m; k++)
//        if (stu[k].num == stu[m].num)
//        {
//            printf("number已经存在，按任意键继续!");
//            fclose(fp);
//            return;
//        }
//    printf("name:");
//    scanf("%s", stu[i + 1].name);
//    printf("elective:");
//    scanf("%lf", &stu[i + 1].elec);
//    printf("experiment:");
//    scanf("%lf", &stu[i + 1].expe);
//    printf("required course:");
//    scanf("%lf", &stu[i + 1].requ);
//    stu[i + 1].sum = stu[i + 1].elec + stu[i + 1].expe + stu[i + 1].requ;
//    printf("插入成功！按任意键返回主界面！");
//    if ((fp = fopen("data.txt", "wb")) == NULL)
//    {
//        printf("不能打开！\n");
//        return;
//    }
//    for (k = 0; k <= m; k++)
//        if (fwrite(&stu[k], LEN, 1, fp) != 1)
//        {
//            printf("不能保存!");
//        }
//    fclose(fp);
//}
//
//void total()
//{
//    FILE* fp;
//    int m = 0;
//    if ((fp = fopen("data.txt", "r+")) == NULL)
//    {
//        printf("文件不存在！\n");
//        return;
//    }
//    while (!feof(fp))
//        if (fread(&stu[m], LEN, 1, fp) == 1)
//            m++;			
//    if (m == 0) 
//    { 
//        printf("no record!\n");
//        fclose(fp); 
//    }
//    printf("这个班级一共有 %d 名学生!\n", m);
//    fclose(fp);
//}
//
//int main()
//{
//    int n;
//    menu();
//    scanf("%d", &n);
//    while (n)
//    {
//        switch (n)
//        {
//        case 1:
//            in();
//            break;
//        case 2:
//            search();
//            break;
//        case 3:
//            del();
//            break;
//        case 4:
//            modify();
//            break;
//        case 5:
//            insert();
//            break;
//        case 6:
//            order();
//            break;
//        case 7:
//            total();
//            break;
//        case 8:
//            show();
//            break;
//        default:
//            break;
//        }
//        menu();
//        scanf("%d", &n);
//    }
//    return 0;
//}
