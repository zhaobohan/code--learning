#define _CRT_SECURE_NO_WARNINGS

struct Book
{
	char name[30];//成员
	char author[20];
	float price;
};

void Print(struct Book * p)
{
	printf("%s %s %.1f\n", (*p).name, (*p).author, (*p).price);
	printf("%s %s %.1f\n", p->name, p->author, p->price);
	//->
	//结构指针->成员名
}

int main()
{
	struct Book b1 = { "C语言", "xxx", 66.5f };//书
	struct Book b2 = { "数据结构", "xx", 88.6f };//书
	Print(&b1);
	Print(&b2);
	return 0;
}


//#include <stdio.h>
//#include <stdlib.h>
//#define N 13
//struct tree 
//{
//    float num;
//    struct tree* Lnode;
//    struct tree* Rnode;
//}*fp[N]; //保存结点
//
//char s[2 * N]; //放前缀码
//
//void inite_node(float f[], int n) 
//{ //生成叶子结点
//    int i;
//    struct tree* pt;
//    for (i = 0; i < n; i++) 
//    {
//        pt = (struct tree*)malloc(sizeof(struct tree));//生成叶子结点
//        pt->num = f[i];
//        pt->Lnode = NULL;
//        pt->Rnode = NULL;
//        fp[i] = pt;
//    }
//}
//
//void sort(struct tree* array[],int n) 
//{ //将第N-n个点插入到已排好序的序列中。
//    int i;
//    struct tree* temp;
//    for (i = N - n; i < N - 1; i++) 
//    {
//        if (array[i]->num > array[i + 1]->num) 
//        {
//            temp = array[i + 1];
//            array[i + 1] = array[i];
//            array[i] = temp;
//        }
//    }
//}
//
//struct tree* construct_tree(float f[], int n) 
//{ //建立树
//    int i;
//    struct tree* pt;
//    for (i = 1; i < N; i++) 
//    {
//        pt = (struct tree*)malloc(sizeof(struct tree));//生成非叶子结点
//        pt->num = fp[i - 1]->num + fp[i]->num;
//        pt->Lnode = fp[i - 1];
//        pt->Rnode = fp[i];
//        fp[i] = pt;//w1+w2
//        sort(fp, N - i);
//    }
//    return fp[N - 1];
//}
//
//void preorder(struct tree* p, int k, char c) 
//{
//    int j;
//    if (p != NULL) 
//    {
//        if (c == 'l')
//            s[k] = '0';
//        else
//            s[k] = '1';
//        if (p->Lnode == NULL) 
//        {
//            //P指向叶子
//            printf("%.2f:  ", p->num);
//            for (j = 0; j <= k; j++)
//                printf("%c", s[j]);
//            putchar('\n');
//        }
//        preorder(p->Lnode, k + 1, 'l');
//        preorder(p->Rnode, k + 1, 'r');
//    }
//}
//
//int main() 
//{
//    float f[N] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41 };
//    struct tree* head;
//    inite_node(f, N); //初始化结点
//    head = construct_tree(f, N);//生成最优树
//    s[0] = 0;
//    preorder(head, 0, 'l');//遍历树
//    return 0;
//}

//#include <stdio.h>
//#define N 5
//#define M 5
//int count = 0, cur = 0, r[N][N];
//int sequence[M];
//void try1(int k)
//{
//    int i, pre = cur;
//    for (i = 0; i < N; i++)
//    {
//        if (r[cur][i])
//        {
//            r[cur][i] = 0;
//            cur = sequence[k] = i;
//            if (k < M) try1(k + 1);
//            r[pre][i] = 1;
//            cur = pre;
//        }
//    }
//}




//#include <stdio.h>
//int r[101][101];
//int main()
//{
//	int flag = 1;
//	int sum1,sum2;
//	int n;
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			scanf("%d", &r[i][j]);
//		}
//	}
//
//	for (int i = 0; i < n && flag; i++)
//	{
//		sum1 = 0;
//		sum2 = 0;
//		for (int j = 0; j < n; j++)
//		{
//			if (r[i][j]) 
//				sum1++;
//		}
//		for (int j = 1; j <= n; j++)
//		{
//			if (r[j][i])
//				sum2++;
//		}
//		if (sum1 % 2 == 0|| sum2 % 2 == 0) 
//			flag = 0;
//	}
//	if (flag == 0)
//		printf("不是欧拉图\n");
//	else
//		printf("是欧拉图\n");
//}


//#include <stdio.h>
//int r[101][101];
//int main()
//{
//	int flag = 1; 
//	int sum;
//	int n;
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			scanf("%d", &r[i][j]);
//		}
//	}
//	for (int i = 0; i < n && flag; i++)
//	{
//		sum = 0;
//		for (int j = 0; j < n; j++)
//		{
//			if (r[i][j])
//				sum++;
//		}
//		if (sum % 2 == 0)
//			flag = 0;
//	}
//	if (flag == 0)
//		printf("不是欧拉图\n");
//	else
//		printf("是欧拉图\n");
//}




//void test1(int arr[])
//{
//	printf("%d\n", sizeof(arr));//(2)
//}
//void test2(char ch[])
//{
//	printf("%d\n", sizeof(ch));//(4)
//}
//int main()
//{
//	int arr[10] = { 0 };
//	char ch[10] = { 0 };
//	printf("%d\n", sizeof(arr));//(1)
//	printf("%d\n", sizeof(ch));//(3)
//	test1(arr);
//	test2(ch);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//	i = a++ && ++b && d++;
//	//i = a++||++b||d++;
//	printf("a = %d\n b = %d\n c = %d\nd = %d\n", a, b, c, d);
//	return 0;
//}