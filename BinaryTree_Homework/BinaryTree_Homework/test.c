#define _CRT_SECURE_NO_WARNINGS

#include "tree.h"
#include "queue.h"

void test1()
{
	char ch[] = "AB##CDF###E##";
	int pos = 0;
	BTNode* tree = BinaryTreeCreate(ch, &pos);
	BinaryTreePrevOrder(tree);
	printf("\n");
	BinaryTreeInOrder(tree);
	printf("\n");
	BinaryTreePostOrder(tree);
	printf("\n");
	BinaryTreeLevelOrder(tree);
	printf("\n");
	printf("全部节点:%d\n", BinaryTreeSize(tree));
	printf("叶子:%d\n", BinaryTreeLeafSize(tree));
	printf("第三层:%d\n", BinaryTreeLevelKSize(tree,3));
	printf("完全二叉树:%d", BinaryTreeComplete(tree));
	BinaryTreeDestory(tree);
	tree = NULL;
}

//测试队列
//void test2()
//{
//	Queue q;
//	QueueInit(&q);
//	QueuePush(&q, 1);
//	QueuePush(&q, 2);
//	QueuePush(&q, 3);
//	QueuePush(&q, 4);
//	QueuePush(&q, 5);
//	printf("Front:%d\n", QueueFront(&q));
//	printf("Back:%d\n", QueueBack(&q));
//	printf("size:%d\n", QueueSize(&q));
//	while (!QueueEmpty(&q))
//	{
//		printf("%d ", QueueFront(&q));
//		QueuePop(&q);
//	}
//	printf("size:%d\n", QueueSize(&q));
//
//}

int main()
{
	//test2();
	test1();

}