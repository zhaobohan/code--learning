#define _CRT_SECURE_NO_WARNINGS

#include "heap.h"

int main()
{
	int arr[] = { 10,50,70,20,15,90 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	HeapSort(arr,sz);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

//int main()
//{
//	Heap hp={0,0,0};
//	HeapCreate(&hp, hp.a, 4);
//	int arr[] = { 10,50,70,20,15,90 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		HeapPush(&hp, arr[i]);
//	}
//	printf("sz=%d\n", sz);
//	printf("hp.size=%d\n", HeapSize(&hp));
//	for (int i = HeapSize(&hp); i > 0; i--)
//	{
//		printf("%d ", HeapTop(&hp));
//		HeapPop(&hp);
//	}
//	printf("\n");
//	printf("Empty?:%d", HeapEmpty(&hp));
//	return 0;
//}