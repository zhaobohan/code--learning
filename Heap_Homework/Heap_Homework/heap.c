#define _CRT_SECURE_NO_WARNINGS

#include "heap.h"

void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	hp->capacity = n;
	hp->size = 0;
	hp->a = (HPDataType*)malloc(sizeof(HPDataType) * hp->capacity);;
}

void HeapDestory(Heap* hp)
{
	assert(hp);
	hp->capacity = 0;;
	hp->size = 0;
	free(hp->a);
	hp->a = NULL;
}

void Swap(HPDataType* p, HPDataType* c)
{
	HPDataType tmp = *p;
	*p = *c;
	*c = tmp;
}

void AdjustUp(HPDataType* a,int child)
{
	assert(a);
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjustDown(HPDataType* a, int size, int parent)
{
	assert(a);
	int child = parent * 2 + 1;
	while (child < size)
	{
		if (a[child] > a[child + 1])
		{
			child++;
		}
		if (child + 1 < size && a[parent] > a[child])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
	
}

void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		HPDataType* tmp = (HPDataType*)realloc(hp->a,sizeof(HPDataType) * hp->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		hp->a = tmp;
		hp->capacity *= 2;
	}
	hp->a[hp->size] = x;
	hp->size++;

	AdjustUp(hp->a, hp->size - 1);
}

void HeapPop(Heap* hp)
{
	assert(hp);
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size - 1, 0);
}

HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->a[0];
}

int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

int HeapEmpty(Heap* hp)
{
	assert(hp);
	if (hp->size == 0)
		return 1;
	return 0;
}

void HeapSort(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		AdjustUp(a, i);
	}
	while (n)
	{
		Swap(&a[0], &a[n-1]);
		AdjustDown(a, n - 1, 0);
		n--;
	}
}