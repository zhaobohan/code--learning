#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define Set 10

void Initboard(char board[][COLS], int rows, int cols, char c);

void Displayboard(char board[][COLS], int row, int col);

void Setmine(char mineboard[][COLS], int row,int col);

void FindSets(char mineboard[][COLS], char showboard[][COLS], int row, int col);


