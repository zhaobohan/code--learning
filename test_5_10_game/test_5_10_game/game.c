#define _CRT_SECURE_NO_WARNINGS
#include "game.h"

void Initboard(char board[][COLS], int rows, int cols, char c)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			board[i][j] = c;
	}
}

void Displayboard(char board[][COLS], int row, int col)
{
	for (int i = 0; i <= row; i++)
	{
		if (i == 0)
			printf("%d|",i);
		else
		    printf("%d ", i);
	}
	printf("\n");
	for (int i = 0; i < row; i++)
	{
		printf("--");
	}
	printf("\n");
	for (int i = 1; i <= row; i++)
	{
		printf("%d|", i);
		for (int j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void Setmine(char mineboard[][COLS], int row, int col)
{
	int num = Set;
	while (num)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (mineboard[x][y] == '0')
		{
			mineboard[x][y] = '1';
			num--;
		}
	}
}

int Count(char board[][COLS], int x, int y)
{
	return (board[x - 1][y] + board[x - 1][y - 1] + board[x - 1][y + 1] + board[x][y - 1] +
		board[x][y + 1] + board[x + 1][y] + board[x + 1][y - 1] + board[x + 1][y + 1]- 8*'0');
}

void FindSets(char mineboard[][COLS], char showboard[][COLS], int row, int col)
{
	int x=0, y=0;
	int nums = row * col - Set;
	while (nums)
	{
		printf("请输入坐标:-->");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
		{
			if (mineboard[x][y] == '1')
			{
				printf("被炸死了\n");
				break;
			}
			else
			{
				int ret = Count(mineboard, x, y);
				showboard[x][y] = ret + '0';
				Displayboard(showboard, ROW, COL);
				nums--;
			}
		}
		else
		{
			printf("坐标不合法，重新输入\n");
		}
	}
	if (nums == 0)
	{
		printf("扫雷成功，游戏结束\n");
	}
	
}




