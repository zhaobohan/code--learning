#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
using namespace std;

typedef int SLDataType;
typedef struct Seqlist
{
	SLDataType* a;
	int size;
	int capacity;
}Seqlist;

void SeqlistInit(Seqlist* s,int capacity=4)
{
	s->a = (SLDataType*)malloc(sizeof(SLDataType) * capacity);
	s->size = 0;
	s->capacity = capacity;
}

int main()
{
	Seqlist sq,sl;
	SeqlistInit(&sl);
	SeqlistInit(&sq, 100);
	return 0;
}


//#include <stdio.h>
//#include <stdlib.h>
//
//typedef int SLDataType;
//typedef struct Seqlist
//{
//	SLDataType* a;
//	int size;
//	int capacity;
//};
//
//void SeqlistInit(Seqlist* s)
//{
//	s->a = (SLDataType*)malloc(sizeof(SLDataType) * 4);
//	s->size = 0;
//	s->capacity = 4;
//}

//#include <iostream>
//using namespace std;
//
//void f(int a = 10, int b = 20, int c = 30)
//{
//	cout << a << " " << b << " " << c << endl;
//}
//
//int main()
//{
//	f();
//	f(1);
//	f(1, 2);
//	f(1, 2, 3);
//	return 0;
//}

//#include <stdio.h>
//
//namespace zbh
//{
//	//定义变量
//	int test = 0;
//
//	//定义函数
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//
//	//定义结构体
//	struct MyStruct
//	{
//		int a;
//		int b;
//	};
//
//	//命名空间可以嵌套
//	namespace free
//	{
//		int print1()
//		{
//			return 1;
//		}
//	}
//}
//
//using zbh::Add;
//
//int main()
//{
//	printf("%d\n", zbh::test);
//	printf("%d\n", Add(1,2));
//	printf("%d", zbh::free::print1());
//}

//#include <stdlib.h>
//#include <stdio.h>
//
//int rand = 0;
//
//int main()
//{
//	printf("%d", rand);
//	return 0;
//}