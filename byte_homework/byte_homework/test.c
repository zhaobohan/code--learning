﻿#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
int main()
{
    unsigned char puc[4];
    struct tagPIM
    {
        unsigned char ucPim1;
        unsigned char ucData0 : 1;
        unsigned char ucData1 : 2;
        unsigned char ucData2 : 3;
    }*pstPimData;
    pstPimData = (struct tagPIM*)puc;
    memset(puc, 0, 4);
    pstPimData->ucPim1 = 2;
    pstPimData->ucData0 = 3;
    pstPimData->ucData1 = 4;
    pstPimData->ucData2 = 5;
    printf("%02x %02x %02x %02x\n", puc[0], puc[1], puc[2], puc[3]);
    return 0;
}


//#pragma pack(4)/*编译选项，表示4字节对齐 平台：VS2013。语言：C语言*/
////假设long 是4个字节
//int main(int argc, char* argv[])
//{
//    struct tagTest1
//    {
//        short a;
//        char d;
//        long b;
//        long c;
//    };
//    struct tagTest2
//    {
//        long b;
//        short c;
//        char d;
//        long a;
//    };
//    struct tagTest3
//    {
//        short c;
//        long b;
//        char d;
//        long a;
//    };
//    struct tagTest1 stT1;
//    struct tagTest2 stT2;
//    struct tagTest3 stT3;
//
//    printf("%d %d %d", sizeof(stT1), sizeof(stT2), sizeof(stT3));
//    return 0;
//}
//#pragma pack()
//

//struct S
//{
//	int data[1000];
//	int num;
//};
//struct S s = { {1,2,3,4}, 1000 };
//
//void print1(struct S s)
//{
//	printf("%d\n", s.num);
//}
//
//void print2(struct S* ps)
//{
//	printf("%d\n", ps->num);
//}
//int main()
//{
//	print1(s); 
//	print2(&s); 
//	return 0;
//}

//#include <stdio.h>
//#pragma pack(8)//设置默认对齐数为8
//struct S1	
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//
//#pragma pack(1)//设置默认对齐数为1
//struct S2
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//
//int main()
//{
//	printf("%d\n", sizeof(struct S1));
//	printf("%d\n", sizeof(struct S2));
//	return 0;
//}

//#include <stdio.h>
//
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//
//int main()
//{
//	printf("%d", (int)sizeof(struct S2));
//	return 0;
//}


//#include <stdio.h>
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//
//int main()
//{
//	printf("%d", (int)sizeof(struct S4));
//	return 0;
//}


//#include <stdio.h>
//
//void test(int (*arr1)[5], int arr2[])
//{
//	;
//}
//
//int main()
//{
//	int arr1[10][5] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 1,2,3,4,5,6 };
//	test(arr1, arr2);
//	return 0;
//}



//模拟实现memmove
//#include <stdio.h>
//
//void* my_memmove(void* dest, void* reso, size_t size)
//{
//	void* ret = dest;
//	if (reso > dest)
//	{
//		while (size--)
//		{
//			*(char*)dest = *(char*)reso;
//			(char*)dest = (char*)dest + 1;
//			(char*)reso = (char*)reso + 1;
//		}
//	}
//	else
//	{
//		while (size--)
//		{
//			*((char*)dest + size) = *((char*)reso + size);
//		}
//	}
//	return ret;
//}
//
//void* my_memcpy(void* arr2, void* arr1, size_t size)
//{
//	void* ret = arr2;
//	while (size--)
//	{
//		*(char*)arr2 = *(char*)arr1;
//		(char*)arr2 = (char*)arr2 + 1;
//		(char*)arr1 = (char*)arr1 + 1;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9 };
//	int arr2[10] = { 0 };
//	my_memmove(arr1+2, arr1, 16);
//	//my_memcpy(arr1 + 2, arr1, 16);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}



//模拟实现memcpy
//#include <stdio.h>
//#include <string.h>
//
//void* my_memcpy(void* arr2, void* arr1, size_t size)
//{
//	while (size--)
//	{
//		*(char*)arr2 = *(char*)arr1;
//		(char*)arr2 = (char*)arr2 + 1;
//		(char*)arr1 = (char*)arr1 + 1;
//	}
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9 };
//	int arr2[20] = { 0 };
//	//memcpy(arr2, arr1, 16);
//	my_memcpy(arr2, arr1, 16);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}



//模拟实现memcpy
//#include <stdio.h>
//#include <string.h>
//
//void* my_memcpy(void* arr2, void* arr1, size_t size)
//{
//	while (size--)
//	{
//		*(char*)arr2 = *(char*)arr1;
//		(char*)arr2 = (char*)arr2 + 1;
//		(char*)arr1 = (char*)arr1 + 1;
//	}
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9 };
//	int arr2[20] = { 0 };
//	//memcpy(arr2, arr1, 16);
//	my_memcpy(arr2, arr1, 16);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//实现一个函数，可以左旋字符串中的k个字符。
//
//例如：
//
//ABCD左旋一个字符得到BCDA
//
//ABCD左旋两个字符得到CDAB

//#include <stdio.h>
//#include <string.h>
//
//void Reverse(char arr[], int n, int len)
//{
//	for (int i = 0; i < n; i++)
//	{
//		char tmp = arr[0];
//		for (int j = 0; j < len-1; j++)
//			arr[j] = arr[j + 1];
//		arr[len-1] = tmp;
//	}
//}
//
//int main()
//{
//	char arr[] = "abcd";
//	int n=0;
//	int len = strlen(arr);
//	scanf("%d", &n);
//	Reverse(arr, n, len);
//	printf("%s\n", arr);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int i, j, n = 0;
//	int a[100][100] = { 0 };
//	scanf("%d", &n);
//	for (i = 0; i < n; i++)
//		a[i][0] = 1;
//	for (i = 1; i < n; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf("%5d", a[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//
//#include<stdio.h>
//
//int main()
//{
//	char k = 'A';
//	for (k = 'A'; k <= 'D'; k++)
//	{
//		if ((k != 'A') + (k == 'C') + (k == 'D') + (k != 'D') == 3)
//		{
//			printf("%c\n", k);
//		}
//	}
//	return 0;
//}




//#include<stdio.h>
//int main()
//{
//	int a = 0, b = 0, c = 0, d = 0, e = 0;
//	for (a = 1; a <= 5; a++)
//	{
//		for (b = 1; b <= 5; b++)
//		{
//			for (c = 1; c <= 5; c++)
//			{
//				for (d = 1; d <= 5; d++)
//				{
//					for (e = 1; e <= 5; e++)
//					{
//						if ((2 == b && 3 != a) || (2 != b && 3 == a) == 1)
//						{
//							if ((2 == b && 4 != e) || (2 != b && 4 == e) == 1)
//							{
//								if ((1 == c && 2 != d) || (1 != c && 2 == d) == 1)
//								{
//									if ((5 == c && 3 != d) || (5 != c && 3 == d) == 1)
//									{
//										if ((4 == e && 1 != a) || (4 != e && 1 == a) == 1)
//										{
//											if (((a != b) && (a != c) && (a != d) && (a != e)) && ((b != c) && (b != d) && (b != e)) && ((c != d) && (c != e)) && ((d != e)))
//
//												printf("%d %d %d %d %d", a, b, c, d, e);
//
//
//										}
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//
//	return 0;
//}




//#include <stdio.h>
//
//int main()
//{
//    char a[1000] = { 0 };
//    int i = 0;
//    for (i = 0; i < 1000; i++)
//    {
//        a[i] = -1 - i;
//    }
//    printf("%d", strlen(a));
//    return 0;
//}

//int main()
//{
//	unsigned char a = 200;
//	unsigned char b = 100;
//	unsigned char c = 0;
//	c = a + b;
//	printf("%d %d", a + b, c);
//	return 0;
//}




//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
//
//void reverse(char* left, char* right)
//{
//	char tmp;
//	while (left < right)
//	{
//		tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	char arr[1001] = "i like beijing.";
//	//scanf("%[\n]s", arr);
//	int len = (int)strlen(arr);
//	reverse(arr, arr + len - 1);
//	printf("%s", arr);
//	char* begin = arr;
//	char* end = arr;
//	while (*end != '\0')
//	{
//		while (*end != ' ' && *end != '\0')
//		{
//			end++;
//		}
//		char* tmp = end;
//		reverse(begin, end);
//		begin = tmp + 1;
//		end = begin;
//	}
//	return 0;
//}


//void reverse(char* left, char* right)
//{
//	char tmp;
//	while (left < right)
//	{
//		tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	char arr[1001] = "i like beijing.";
//	//scanf("%[\n]s", arr);
//	int len = (int)strlen(arr);
//	reverse(arr, arr + len - 1);
//	printf("%s", arr);
//	char* begin = arr;
//	char* end = arr;
//	while (*end!='\0')
//	{
//		while (*end != ' ' && *end != '\0')
//		{
//			end++;
//		}
//		char* tmp = end;
//		reverse(begin, end);
//		begin = tmp + 1;
//		end = begin;
//	}
//	return 0;
//}



//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int j = 10;
//	int* const p = &i;
//	*p = 20;
//	p = &j;
//	return 0;
//}


//找到规律是关键，看作一条正斜杠和反斜杠
//#include <stdio.h>
//int main()
//{
//    int m = 0;
//    while (scanf("%d", &m) != EOF)
//    {
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (i == j || i == m + 1 - j)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//            }
//            printf("\n");
//        }
//        printf("\n");
//    }
//    return 0;
//}


//#include <stdio.h>
//int main()
//{
//	int i = 10;
//	int k = 20;
//	 int const* p = &i;
//	p = &k;
//	*p = 20;
//	return 0;
//}

//
//调整数组使奇数全部都位于偶数前面。
//
//
//
//题目：
//
//输入一个整数数组，实现一个函数，
//
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//
//所有偶数位于数组的后半部分。
//
// 
//#include <stdio.h>
//#include <string.h>
//
//void Order(int arr[], int sz)
//{
//	int tmp[1001];
//	int j = sz;
//	int k = 0;
//	for (int i = 0; i < sz; i++)
//	{
//		if (arr[i] % 2 == 1)//奇数
//		{
//			tmp[k] = arr[i];
//			k++;
//		}
//		else
//		{
//			tmp[j-1] = arr[i];
//			j--;
//		}
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		arr[i] = tmp[i];
//	}
//}
//
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Order(arr, sz);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//

// 
//#include <stdio.h>
//#include <string.h>
//
//void Order(int arr[], int sz)
//{
//	int tmp[1001];
//	int j = sz;
//	int k = 0;
//	for (int i = 0; i < sz; i++)
//	{
//		if (arr[i] % 2 == 1)//奇数
//		{
//			tmp[k] = arr[i];
//			k++;
//		}
//		else
//		{
//			tmp[j-1] = arr[i];
//			j--;
//		}
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		arr[i] = tmp[i];
//	}
//}
//
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Order(arr, sz);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//


//喝汽水问题
//1瓶汽水1元，2个空瓶可以换一瓶汽水
//#include <stdio.h>
//
//int main()
//{
//	int num = 20;
//	int bot = 0, top = 0;
//	bot = num;
//	top = bot;
//	while (top != 0)
//	{
//		bot = bot + top / 2;
//		top = top / 2;
//	}
//	printf("%d", bot);
//	return 0;
//}




//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
//
//例如：2 + 22 + 222 + 2222 + 22222

//#include <stdio.h>
//#include <math.h>
//
//int Nummore(int j, int i)
//{
//	double ret = j;
//	for (double m = 1; m < i; m++)
//	{
//		ret = ret + j * pow(10, m);
//	}
//	return (int)ret;
//}
//
//int Sn(int j)
//{
//	int sum = 0;
//	for (int i = 1; i <= 5; i++)
//	{
//		sum = sum + Nummore(j, i);
//	}
//	return sum;
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	int sum = Sn(i);
//	printf("%d", sum);
//	return 0;
//}

//求出0～100000之间的所有“水仙花数”并输出。
//
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，
//如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。

//#include <math.h>
//#include <stdio.h>
//
//void Judge(int i)
//{
//	int power = 0, sum=0;
//	int tmp1 = i;
//	int tmp2 = i;
//	while (tmp1)
//	{
//		power++;
//		tmp1 /= 10;
//	}
//	while (tmp2)
//	{
//		sum = sum + (int)pow((double)(tmp2 % 10), (double)power);
//		tmp2 = tmp2 / 10;
//	}
//	if (sum == i)
//		printf("%d ", i);
//}
//
//int main()
//{
//	for (int i = 0; i < 100000; i++)
//	{
//		Judge(i);
//	}
//	return 0;
//}



//#include <stdio.h>
//int main()
//{
//    int a, b, c;
//    for (a = 1; a <= 7; a++)              
//    {
//        for (b = 1; b <= 7 - a; b++)     
//            printf(" ");
//        for (c = 1; c <= 2 * a - 1; c++) 
//            printf("*");
//        printf("\n");                              
//    }
//    for (a = 1; a <= 6; a++)           
//    {
//        for (b = 1; b <= a; b++)
//            printf(" ");
//        for (c = 1; c <= 13 - 2 * a; c++) 
//            printf("*");
//        printf("\n");                      
//    }
//    return 0;
//}



//写一个函数，可以逆序一个字符串的内容。
//#include <stdio.h>
//
//void Reverse(char* str)
//{
//	int cout = 0;
//	char* p = str;
//	while (*p != '\0')
//	{
//		cout++;
//		p++;
//	}
//	for (int i = cout; i >= 0 ; i--)
//	{
//		printf("%c", *(str + i));
//	}
//}
//
//int main()
//{
//	char str[] = "abcdef";
//	Reverse(str);
//	return 0;
//}

//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//
//arr是一个整形一维数组。
//#include <stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}




//#include <stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int count = 0;
//    scanf("%d %d", &a, &b);
//    for (int i = 0; i < 32; i++)
//    {
//        if ((a % 2) != (b % 2))
//        {
//            count++;
//        }
//        a /= 2;
//        b /= 2;
//    }
//    printf("%d", count);
//
//    return 0;
//}

//int main() 
//{
//	int num = 0;
//	scanf("%d", &num);
//	printf("奇数位：");
//	for (int i = 31; i >= 1; i -= 2) 
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//	printf("偶数位：");
//	for (int i = 30; i >= 0; i -= 2) 
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//	return 0;
//}

//int main() 
//{
//	int num = 0;
//	scanf("%d", &num);
//	printf("奇数位：");
//	for (int i = 31; i >= 1; i -= 2) 
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//	printf("偶数位：");
//	for (int i = 30; i >= 0; i -= 2) 
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//	return 0;
//}
//int main() 
//{
//	int num = 0;
//	scanf("%d", &num);
//	printf("奇数位：");
//	for (int i = 31; i >= 1; i -= 2) 
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//	printf("偶数位：");
//	for (int i = 30; i >= 0; i -= 2) 
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//	return 0;
//}


//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
//#include <stdio.h>
//int main()
//{
//	int a,arr[100];
//	int i = 0;
//	scanf("%d", &a);
//	while (a)
//	{
//		arr[i++] = a % 2;
//		a = a / 2;
//	}
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("奇数位:");
//	
//	return 0;
//}



//统计二进制中1的个数
//#include <stdio.h>
//int main()
//{
//	int a,b,c;
//	int count = 0;
//	scanf("%d", &a);
//	if (a < 0)
//	{
//		a = -a;
//		count++;
//	}
//	while (a)
//	{
//		if (a % 2 == 1)	
//			count++;
//		a = a/2;
//	}
//	printf("%d", count);
//	return 0;
//}


//不允许创建临时变量，交换两个整数的内容
//#include <stdio.h>
//int main()
//{
//	int a, b;
//	scanf("%d %d",&a,&b);
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("%d %d", a, b);
//	return 0;
//}




//实现一个对整形数组的冒泡排序
//#include <stdio.h>
//
//void Bubble_sort(int arr[], int sz)
//{
//	int tmp = 0;
//	for (int i = 0; i < sz - 1; i++)
//	{
//		for (int j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,6,5,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("Before:\n");
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	printf("\nAfter:\n");
//	Bubble_sort(arr,sz);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



////创建一个整形数组，完成对数组的操作
////实现函数init() 初始化数组为全0
////实现print()  打印数组的每个元素
////实现reverse()  函数完成数组元素的逆置。
////要求：自己设计以上函数的参数，返回值。
//#define MAX 10
//
//#include <stdio.h>
//
//
//void Init(int arr[],int max)
//{
//	for (int i = 0; i < max; i++)
//		arr[i] = 0;
//}
//
//void Print(int arr[], int max)
//{
//	for (int i = 0; i < max; i++)
//		printf("%d ", arr[i]);
//	printf("\n");
//}
//
//void Reverse(int arr[], int max)
//{
//	int tmp[MAX];
//	for (int i = 0; i < max; i++)
//		tmp[i] = arr[i];
//	for (int j = max - 1; j >= 0; j--)
//		arr[j] = tmp[max - j - 1];
//}
//
//int main()
//{
//	int arr[MAX]={1,2,3,4,5,6,7,8,9,10};
//	printf("进行打印函数:\n");
//	Print(arr,MAX);
//	printf("进行逆序函数:\n");
//	Reverse(arr,MAX);
//	printf("进行初始化函数:\n");
//	Init(arr, MAX);
//	Print(arr, MAX);
//	return 0;
//}



//数组交换
//#include <stdio.h>
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 10,9,8,7,6,5,4,3,2,1 };
//	int tmp[10] = { 0 };
//	printf("交换前:\n");
//	for (int i = 0; i < 10; i++)
//		printf("%d ", arr1[i]);
//	printf("\n");
//	for (int i = 0; i < 10; i++)
//		printf("%d ", arr2[i]);
//	printf("\n");
//	for (int i = 0; i < 10; i++)
//	{
//		tmp[i] = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp[i];
//	}
//	printf("交换后:\n");
//	for (int i = 0; i < 10; i++)
//		printf("%d ", arr1[i]);
//	printf("\n");
//	for (int i = 0; i < 10; i++)
//		printf("%d ", arr2[i]);
//	return 0;
//}

//作业标题(677)
//乘法口诀表
//
//作业内容
//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。

//#include <stdio.h>
//
//void Chart(int i)
//{
//	for (int j = 1; j <= i; j++)
//	{
//		for (int k = 1; k <= j; k++)
//		{
//			printf("%d*%d=%d ", k, j, j * k);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	Chart(i);
//	return 0;
//}



//描述
//KiKi非常喜欢网购，在一家店铺他看中了一件衣服，他了解到，如果今天是“双11”（11月11日）则这件衣服打7折，“双12” （12月12日）则这件衣服打8折，如果有优惠券可以额外减50元（优惠券只能在双11或双12使用），求KiKi最终所花的钱数。
//
//数据范围：衣服价格满足
//1
//≤
//�
//�
//�
//≤
//100000
//
//1≤val≤100000
//输入描述：
//一行，四个数字，第一个数表示小明看中的衣服价格，第二和第三个整数分别表示当天的月份、当天的日期、第四个整数表示是否有优惠券（有优惠券用1表示，无优惠券用0表示）。 注：输入日期保证只有“双11”和“双12”。
//输出描述：
//一行，小明实际花的钱数（保留两位小数）。（提示：不要指望商家倒找你钱）

//#include <stdio.h>
//
//int main()
//{
//    int month, day, ret;
//    float spend, discout;
//    scanf("%f %d %d %d", &spend, &month, &day, &ret);
//    if (month == 11 && day == 11)
//        discout = 0.7;
//    else if (month == 12 && day == 12)
//        discout = 0.8;
//    else
//        discout = 1.0;
//    if (ret == 1 && spend * discout - 50 >= 0)
//        printf("%.2f", spend * discout - 50);
//    else if (ret == 1 && spend * discout - 50 < 0)
//        printf("0.00");
//    else if (ret == 0 && spend * discout >= 0)
//        printf("%.2f", spend * discout);
//    else if (ret == 0 && spend * discout < 0)
//        printf("0.00");
//    return 0;
//}


//描述
//KiKi想判断输入的字符是不是字母，请帮他编程实现。
//
//输入描述：
//多组输入，每一行输入一个字符。
//输出描述：
//针对每组输入，输出单独占一行，判断输入字符是否为字母，输出内容详见输出样例。
//#include <stdio.h>
//
//int main()
//{
//    char input = 0;
//    while (scanf("%c", &input) != EOF)
//    {
//        if ((input >= 'a' && input <= 'z')|| (input >= 'A' && input <= 'Z'))
//            printf("%c is an alphabet.\n", input);
//        else
//            printf("%c is not an alphabet.\n", input);
//        getchar();
//    }
//    return 0;
//}

//描述
//小乐乐获得4个最大数，请帮他编程找到最大的数。
//输入描述：
//一行，4个整数，用空格分开。
//输出描述：
//一行，一个整数，为输入的4个整数中最大的整数。
//#include <stdio.h>
//
//int main()
//{
//    int max = -10000;
//    int ret = 0;
//    for (int i = 0; i < 4; i++)
//    {
//        scanf("%d", &ret);
//        if (ret > max)
//            max = ret;
//    }
//    printf("%d", max);
//    return 0;
//}


//作业内容
//BC112 - 小乐乐求和
//﻿﻿点击题目链接，做题
//
//提交牛客网提交代码截图和提交通过的2张截图
//#include <stdio.h>
//
//int main()
//{
//    int ret = 0;
//    scanf("%d", &ret);
//    long long sum = 0;
//    for (int i = 1; i <= ret; i++)
//    {
//        sum = sum + i;
//    }
//    printf("%lld", sum);
//    return 0;
//}


//作业标题(664)
//二分查找
//
//作业内容
//编写代码在一个整形有序数组中查找具体的某个数
//
//要求：找到了就打印数字所在的下标，找不到则输出：找不到。

//#include <stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int len = sizeof(arr)/sizeof(arr[0]);
//	int left = 0, right = len - 1;
//	int ret,mid=(left+right)/2,flag=0;
//	scanf("%d", &ret);
//	while(left<=right)
//	{
//		if (ret == arr[mid])
//		{
//			printf("ok,%d", mid);
//			flag = 1;
//			break;
//		}
//		else if (ret > arr[mid])//右侧
//		{
//			left = mid+1;
//			mid = (left + right) / 2;
//		}
//		else
//		{
//			right = mid-1;
//			mid = (left + right) / 2;
//		}
//	}
//	if (flag == 0)
//		printf("找不到\n");
//	return 0;
//}


//作业标题(660)
//数9的个数
//
//作业内容
//编写程序数一下 1到 100 的所有整数中出现多少个数字9

//#include <stdio.h>
//int main()
//{
//	int cout = 0;
//	for (int i = 1; i <= 100; i++)
//	{
//		if (i / 10 == 9)
//			cout++;
//		if (i % 10 == 9)
//			cout++;
//	}
//	printf("%d", cout);
//}



//作业标题(659)
//分数求和
//
//作业内容
//计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值
//打印出结果

//#include <stdio.h>
//int main()
//{
//	int i = 1;
//	float j = 1;
//	float ret, sum=0;
//	while (j <= 100)
//	{
//		ret = i / j;
//		sum = sum + ret;
//		i = -i;
//		j++;
//	}
//	printf("%f", sum);
//	return 0;
//}


//作业标题(658)
//求最大值
//
//作业内容
//求10 个整数中最大值

//#include <stdio.h>
//int main()
//{
//	int arr[10];
//	for (int i = 0; i < 10; i++)
//		scanf("%d", &arr[i]);
//	int max = -10000;
//	int j = 0;
//	while (j < 10)
//	{
//		if (arr[j] > max)
//			max = arr[j];
//		j++;
//	}
//	printf("max=%d", max);
//	return 0;
//}

//作业标题(657)
//乘法口诀表
//
//作业内容
//在屏幕上输出9 * 9乘法口诀表

//#include <stdio.h>
//int main()
//{
//	int i, j;
//	for (i = 1; i <= 9;i++)
//	{
//		for (j = 1; j <= 9; j++)
//		{
//			printf("%dx%d=%d  ", j, i, i * j);
//			if (j == i)
//			{
//				printf("\n");
//				break;
//			}
//		}
//	}
//	return 0;
//}


//作业标题(637)
//求两个数的较大值
//作业内容
//写一个函数求两个整数的较大值
//如：
//输入：10 20
//输出较大值：20
//#include <stdio.h>
//int main()
//{
//	int a, b;
//	scanf("%d %d", &a, &b);
//	if (a > b)
//	printf("%d\n", a);
//	else
//	printf("%d\n", b);
//	return 0;
//}

//#include <stdio.h>
//int sum(int a)
//{
//    int c = 0;
//    static int b = 3;
//    c += 1;
//    b += 2;
//    return (a + b + c);
//}
//int main()
//{
//    int i;
//    int a = 2;
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d,", sum(a));
//    }
//}