#define _CRT_SECURE_NO_WARNINGS

#include "listnode.h"

ListNode* BuyListNode(DataType x)
{
	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	newnode->next = NULL;
	newnode->data = x;
	return newnode;
}

DataType Buynewdata()
{
	DataType newdata;
	printf("请依次输入编号 姓名 性别 电话 地址\n");
	scanf("%s %s %s %s %s",
		newdata.num, newdata.name, newdata.sex, newdata.phone, newdata.addr);
	return  newdata;
}

void ListNodePush(ListNode** phead)
{
	assert(phead);
	ListNode* newnode = BuyListNode(Buynewdata());
	if (*phead == NULL)
	{
		*phead = newnode;
	}
	else
	{
		newnode->next = *phead;
		*phead = newnode;
	}
}

void ListNodePrint(ListNode** phead)
{
	ListNode* cur = *phead;
	printf("%-5s%-10s%-8s%-13s%-31s\n",
		"编号", "姓名", "性别", "电话", "地址");
	while (cur)
	{
		printf("%-5s%-10s%-8s%-13s%-31s\n",
			cur->data.num, cur->data.name, cur->data.sex,
			cur->data.phone, cur->data.addr);
		cur = cur->next;
	}
}

int ListNodeFind(ListNode* head,const char* Findname)
{
	ListNode* cur = head;
	while (cur)
	{
		if (strcmp(Findname, cur->data.name) == 0)
		{
			printf("找到了,该人的信息如下\n");
			printf("%-5s%-10s%-8s%-13s%-31s\n",
				"编号", "姓名", "性别", "电话", "地址");
			printf("%-5s%-10s%-8s%-13s%-31s\n",
				cur->data.num, cur->data.name, cur->data.sex,
				cur->data.phone, cur->data.addr);
			return 1;
		}
		else
		{
			cur = cur->next;
		}
	}
	printf("找不到信息\n");
	return 0;
}

void ListNodePop(ListNode** phead,const char* Popname)
{
	assert(*phead);
	assert(phead);
	if (ListNodeFind(*phead, Popname))
	{
		ListNode* Findnode = *phead;
		ListNode* prev = *phead;
		while (strcmp(Findnode->data.name,Popname)!=0)
		{
			prev = Findnode;
			Findnode = Findnode->next;
		}
		prev->next = Findnode->next;
		free(Findnode);
		Findnode = NULL;
		printf("删除该人信息成功\n");
		return;
	}
	else
	{
		printf("找不到该人信息\n");
		return;
	}
}