#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

typedef  struct 
{
    char  num[5];    //编号
    char  name[9];   //姓名
    char  sex[3];     //性别
    char  phone[13];  //电话
    char  addr[31];   //地址
}DataType;

typedef struct node 
{      
    DataType data;
    struct node* next;
}ListNode;

//头节点用head
//定义一个指向结点的指针变量 ListNode* p;

//1. 通讯录链表的建立
//2. 通讯者结点的插入
//3. 通讯者结点的查询
//4. 通讯者结点的删除
//5. 通讯录链表的输出

ListNode* BuyListNode(DataType x);

void ListNodePush(ListNode** phead);

DataType Buynewdata();

void ListNodePrint(ListNode** phead);

int ListNodeFind(ListNode* head,const char* Findname);

void ListNodePop(ListNode** phead,const char* Popname);