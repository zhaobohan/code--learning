#define _CRT_SECURE_NO_WARNINGS

#include "listnode.h"

void menu()
{
	printf("*********************************************\n");
	printf("****1.建立信息表************** 2.插入信息 ****\n");
	printf("****3.查询信息  ************** 4.删除信息 ****\n");
	printf("****5.打印信息  ************** 0.退出     ****\n");
	printf("*********************************************\n");
}

void SetupListNode(ListNode** phead)
{
	int num = 0;
	printf("建立链表成功，开始录入信息\n");
	printf("输入要录取信息的个数->");
	scanf("%d", &num);
	while (num--)
	{
		ListNodePush(phead);
	}
}

void FindFunction(ListNode* head)
{
	char Findname[20] = {0};	
	printf("输入要查找的人名");
	scanf("%s", Findname);
	ListNodeFind(head, Findname);
}

void PopFunction(ListNode** phead)
{
	char Popname[20] = {0};
	printf("输入要查找的人名");
	scanf("%s", Popname);
	ListNodePop(phead, Popname);
}



//int main()
//{
//	menu();
//	int input = 0;
//	ListNode* head = NULL;
//	do
//	{
//		printf("请选择->");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			SetupListNode(&head);
//			break;
//		case 2:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				ListNodePush(&head);
//				break;
//			}
//		case 3:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				FindFunction(head);
//				break;
//			}
//		case 4:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				PopFunction(&head);
//				break;
//			}
//		case 5:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				ListNodePrint(&head);
//				break;
//			}
//		case 0:
//			break;
//		default:
//			printf("请重新选择\n");
//			break;
//		}
//		menu();
//	} while (input);
//	return 0;
//}