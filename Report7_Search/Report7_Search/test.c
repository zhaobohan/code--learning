#define _CRT_SECURE_NO_WARNINGS


//#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
//
//
//typedef struct
//{
//	int num;
//	char name[50];
//	int score_G;
//	int score_C;
//	int score_S;
//}SLDataType;
//
//typedef struct
//{
//	SLDataType* elem;
//	int size;
//	int capacity;
//}SSTable;
//
//void CheckCapacity(SSTable* ps)
//{
//	if (ps->capacity <= ps->size)
//	{
//		int newcapacity = ps->capacity * 2;
//		SLDataType* tmp = (SLDataType*)realloc(ps->elem, sizeof(SLDataType) * newcapacity);
//		if (tmp == NULL)
//		{
//			perror("realloc fail");
//			return;
//		}
//		ps->capacity = newcapacity;
//		ps->elem = tmp;
//	}
//}
//
//void SeqListInit(SSTable* ps)
//{
//	ps->size = 0;
//	ps->capacity = 8;
//	ps->elem = (SLDataType*)malloc(ps->capacity * sizeof(SLDataType));
//	if (ps->elem == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//}
//
//void CreateList(SSTable* ps)
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen fail");
//		return;
//	}
//	ps->size = 10;
//	CheckCapacity(ps);
//	printf("\n学号  姓名  高等数学  C程序设计  数据结构\n");
//	for (int i = 0; i < ps->size; i++)
//	{
//		fscanf(pf,"%d", &ps->elem[i].num);
//		fscanf(pf,"%s", ps->elem[i].name);
//		fscanf(pf,"%d", &ps->elem[i].score_G);
//		fscanf(pf,"%d", &ps->elem[i].score_C);
//		fscanf(pf,"%d", &ps->elem[i].score_S);
//	}
//}
//
//void Search_name(SSTable* ps)
//{
//	int i = 0;
//	char name[50];
//	printf("请输入要查找的姓名->");
//	scanf("%s", name);
//	for (i = 0; i < ps->size; i++)
//	{
//		if (strcmp(name, ps->elem[i].name) == 0)
//		{
//			printf("%d", ps->elem[i].num);
//			printf("%5s", ps->elem[i].name);
//			printf("%5d", ps->elem[i].score_G);
//			printf("%5d", ps->elem[i].score_C);
//			printf("%5d", ps->elem[i].score_S);
//			break;
//		}
//		else
//		{
//			printf("无相关信息！");
//		}
//	}
//}
//
//void Search_score(SSTable* ps)
//{
//	int score_S;
//	printf("\n请输入要查找的数据结构成绩：\n");
//	scanf("%d", &score_S);
//	int low = 0;
//	int high = ps->size - 1;
//	int mid = (low + high) / 2;
//	while (low <= high)
//	{
//		mid = (low + high) / 2;
//
//		if (score_S == ps->elem[mid].score_S)
//		{
//			printf("%4d", ps->elem[mid].num);
//			printf("%5s", ps->elem[mid].name);
//			printf("%5d", ps->elem[mid].score_G);
//			printf("%5d", ps->elem[mid].score_C);
//			printf("%5d", ps->elem[mid].score_S);
//			break;
//		}
//		else if (score_S < ps->elem[mid].score_S)
//		{
//			high = mid - 1;
//		}
//		else
//		{
//			low = mid + 1;
//		}
//
//	}
//	if (high < low)
//	{
//		printf("无相关信息！");
//	}
//}
//
//void sort_score_S(SSTable* ps)
//{
//	int i, j;
//	SLDataType temp;
//	for (i = 0; i < ps->size - 1; i++)
//	{
//		for (j = 0; j < ps->size - 1 - i; j++)
//		{
//			if (ps->elem[j].score_S > ps->elem[j+1].score_S)
//			{
//				temp = ps->elem[j+1];
//				ps->elem[j+1] = ps->elem[j];
//				ps->elem[j] = temp;
//			}
//		}
//	}
//	printf("\n将数据结构成绩按由小到大进行排序：\n");
//	for (i = 0; i < ps->size; i++)
//	{
//		printf("%5d", ps->elem[i].num);
//		printf("%10s", ps->elem[i].name);
//		printf("%5d", ps->elem[i].score_G);
//		printf("%5d", ps->elem[i].score_C);
//		printf("%5d", ps->elem[i].score_S);
//		printf("\n");
//	}
//}
//
//void menu()
//{
//	printf("\n---------------1.按姓名查找---------------\n");
//	printf("\n-----------2.按数据结构成绩查找------------\n");
//	printf("\n---------------0.退出查找！---------------\n");
//}
//
//int main()
//{
//	SSTable s;
//	SeqListInit(&s);
//	int input = 0;
//	menu();
//	CreateList(&s);
//	sort_score_S(&s);
//	do
//	{
//		menu();
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Search_name(&s);
//			break;
//		case 2:
//			Search_score(&s);
//			break;
//		case 0:
//			break;
//		default:
//			printf("该关键字非法！");
//			break;
//		}
//	} while (input);
//	return 0;
//}




#include<stdio.h>
#include<string.h>
#include<stdlib.h>


typedef struct
{
	int num;
	char name[50];
	int score_G;
	int score_C;
	int score_S;
}SLDataType;

typedef struct
{
	SLDataType* elem;
	int size;
	int capacity;
}SSTable;

void CheckCapacity(SSTable* ps)
{
	if (ps->capacity <= ps->size)
	{
		int newcapacity = ps->capacity * 2;
		SLDataType* tmp = (SLDataType*)realloc(ps->elem, sizeof(SLDataType) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		ps->capacity = newcapacity;
		ps->elem = tmp;
	}
}

void SeqListInit(SSTable* ps)
{
	ps->size = 0;
	ps->capacity = 8;
	ps->elem = (SLDataType*)malloc(ps->capacity * sizeof(SLDataType));
	if (ps->elem == NULL)
	{
		perror("malloc fail");
		return;
	}
}

void CreateList(SSTable* ps)
{
	printf("输入线性表的长度->");
	scanf("%d", &ps->size);
	CheckCapacity(ps);
	printf("请输入线性表的内容：");
	printf("\n学号  姓名  高等数学  C程序设计  数据结构\n");
	for (int i = 0; i < ps->size; i++)
	{
		scanf("%d", &ps->elem[i].num);
		scanf("%s", ps->elem[i].name);
		scanf("%d", &ps->elem[i].score_G);
		scanf("%d", &ps->elem[i].score_C);
		scanf("%d", &ps->elem[i].score_S);
	}
}

void Search_name(SSTable* ps)
{
	int i = 0;
	char name[50];
	printf("请输入要查找的姓名->");
	scanf("%s", name);
	for (i = 0; i < ps->size; i++)
	{
		if (strcmp(name, ps->elem[i].name) == 0)
		{
			printf("%d", ps->elem[i].num);
			printf("%5s", ps->elem[i].name);
			printf("%5d", ps->elem[i].score_G);
			printf("%5d", ps->elem[i].score_C);
			printf("%5d", ps->elem[i].score_S);
			break;
		}
		else
		{
			printf("无相关信息！");
		}
	}
}

void Search_score(SSTable* ps)
{
	int score_S;
	printf("\n请输入要查找的数据结构成绩：\n");
	scanf("%d", &score_S);
	int low = 0;
	int high = ps->size - 1;
	int mid = (low + high) / 2;
	while (low <= high)
	{
		mid = (low + high) / 2;

		if (score_S == ps->elem[mid].score_S)
		{
			printf("%4d", ps->elem[mid].num);
			printf("%5s", ps->elem[mid].name);
			printf("%5d", ps->elem[mid].score_G);
			printf("%5d", ps->elem[mid].score_C);
			printf("%5d", ps->elem[mid].score_S);
			break;
		}
		else if (score_S < ps->elem[mid].score_S)
		{
			high = mid - 1;
		}
		else
		{
			low = mid + 1;
		}

	}
	if (high < low)
	{
		printf("无相关信息！");
	}
}

void sort_score_S(SSTable* ps)
{
	int i, j;
	SLDataType temp;
	for (i = 0; i < ps->size - 1; i++)
	{
		for (j = 0; j < ps->size - 1 - i; j++)
		{
			if (ps->elem[j + 1].score_S > ps->elem[j].score_S)
			{
				temp = ps->elem[j + 1];
				ps->elem[j + 1] = ps->elem[j];
				ps->elem[j] = temp;
			}
		}
	}
	printf("\n将数据结构成绩按由小到大进行排序：\n");
	for (i = 0; i < ps->size; i++)
	{
		printf("%5d", ps->elem[i].num);
		printf("%5s", ps->elem[i].name);
		printf("%5d", ps->elem[i].score_G);
		printf("%5d", ps->elem[i].score_C);
		printf("%5d", ps->elem[i].score_S);
		printf("\n");
	}
}

void menu()
{
	printf("\n---------------1.按姓名查找---------------\n");
	printf("\n-----------2.按数据结构成绩查找------------\n");
	printf("\n---------------0.退出查找！---------------\n");
}

int main()
{
	SSTable s;
	SeqListInit(&s);
	int input = 0;
	menu();
	CreateList(&s);
	sort_score_S(&s);
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			Search_name(&s);
			break;
		case 2:
			Search_score(&s);
			break;
		case 0:
			break;
		default:
			printf("该关键字非法！");
			break;
		}
	} while (input);
	return 0;
}