#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>


typedef struct ListNode
{
    int val;
    struct ListNode* next;
}ListNode;

// 0000 0000 0000 0000 0000 0000 0000 1010
//   0    0    0    0    0    0    0    a

//代码1
#include <stdio.h>
int check_sys()
{
    int i = 1;
    return (*(char*)&i);
}
int main()
{
    int m = 10000;
    int n = 20;
    int ret = check_sys();
    if (ret == 1)
    {
        printf("小端\n");
    }
    else
    {
        printf("大端\n");
    }
    return 0;
}

//
//
//#include <stddef.h>
//#include <stdio.h>
//#define OFFSETOF(struct_name,struct_member) (size_t)&(((struct_name*)0)->struct_member)
//#pragma pack(1)
//struct address {
//    char name[50];
//    int phone;
//    char street[50];
//};
//
// 0-39 
//
//int main()
//{
//    printf("address 结构中的 name 偏移 = %d 字节。\n",
//        OFFSETOF(struct address, name));
//
//    printf("address 结构中的 street 偏移 = %d 字节。\n",
//        OFFSETOF(struct address, street));
//
//    printf("address 结构中的 phone 偏移 = %d 字节。\n",
//        OFFSETOF(struct address, phone));
//    printf("%d\n", sizeof(struct address));
//    return(0);
//}


//struct ListNode* reverseList(struct ListNode* head)
//{
//    if (head == NULL)
//    {
//        return NULL;
//    }
//    struct ListNode* newhead = NULL;
//    struct ListNode* cur = head;
//    while (cur)
//    {
//        struct ListNode* newnode = (struct ListNode*)malloc(sizeof(struct ListNode));
//        newnode->val = cur->val;
//        newnode->next = NULL;
//        if (newhead == NULL)
//        {
//            newhead = newnode;
//        }
//        else
//        {
//            newnode->next = newhead;
//            newhead = newnode;
//        }
//        cur = cur->next;
//    }
//    return newhead;
//}

//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2)
//{
//    struct ListNode* cur1 = list1;
//    struct ListNode* cur2 = list2;
//    struct ListNode* head = NULL;
//    if (cur1 == NULL)
//    {
//        return cur2;
//    }
//    if (cur2 == NULL)
//    {
//        return cur1;
//    }
//    struct ListNode* next1 = NULL;
//    struct ListNode* next2 = NULL;
//    while (cur1 && cur2)
//    {
//        next1 = cur1->next;
//        next2 = cur2->next;
//        if (cur1->val < cur2->val)
//        {
//            cur1->next = head;
//            head = cur1;
//
//            cur1 = next1;
//        }
//        else
//        {
//            cur2->next = head;
//            head = cur2;
//
//            cur2 = next1;
//        }
//    }
//    while (cur1)
//    {
//        next1 = cur1->next;
//
//        cur1->next = head;
//        head = cur1;
//
//        cur1 = next1;
//    }
//    while (cur2)
//    {
//        next2 = cur2->next;
//
//        cur2->next = head;
//        head = cur2;
//
//        cur2 = next2;
//    }
//    return head;
//}

//ListNode* partition(ListNode* pHead, int x)
//{
//    // write code here
//    ListNode* list1head = NULL;
//    ListNode* list1tail = NULL;
//    ListNode* list2head = NULL;
//    ListNode* list2tail = NULL;
//    ListNode* cur = pHead;
//    while (cur)
//    {
//        if (cur->val < x)
//        {
//            if (list1tail == NULL)
//            {
//                list1tail = list1head = cur;
//            }
//            else
//            {
//                list1tail->next = cur;
//                list1tail = list1tail->next;
//            }
//        }
//        else
//        {
//            if (list2tail == NULL)
//            {
//                list2tail = list2head = cur;
//            }
//            else
//            {
//                list2tail->next = cur;
//                list2tail = list2tail->next;
//            }
//        }
//        cur = cur->next;
//    }
//    list1tail->next = list2head;
//    list2tail->next = NULL;
//    pHead = list1head;
//    return pHead;
//}
//
//struct ListNode* detectCycle(struct ListNode* head)
//{
//    struct ListNode* fast = head;
//    struct ListNode* slow = head;
//    struct ListNode* meet = NULL;
//    while (fast && fast->next)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//        if (slow == fast)
//        {
//            meet = fast;
//        }
//    }
//    if (meet == NULL)
//    {
//        return NULL;
//    }
//    while (head != meet)
//    {
//        meet = meet->next;
//        head = head->next;
//    }
//    return head;
//}
//
//int main()
//{
//    struct ListNode* n1 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n1);
//    struct ListNode* n2 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n2);
//    struct ListNode* n3 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n3);
//    struct ListNode* n4 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n4);
//    struct ListNode* n5 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n5);
//
//    n1->val = 3;
//    n2->val = 2;
//    n3->val = 0;
//    n4->val = -4;
//
//    n1->next = n2;
//    n2->next = n3;
//    n3->next = n4;
//    n4->next = n2;
//
//    struct ListNode* n6 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n6);
//    struct ListNode* n7 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n7);
//    struct ListNode* n8 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n8);
//    struct ListNode* n9 = (struct ListNode*)malloc(sizeof(struct ListNode));
//    assert(n9);
//
//    n6->val = 2;
//    n7->val = 3;
//    n8->val = 6;
//    n9->val = 7;
//
//    n6->next = n7;
//    n7->next = n8;
//    n8->next = n9;
//    n9->next = NULL;
//
//    struct ListNode* newhead = detectCycle(n1);
//    // 5 2 8 4 9
//    // 5 2 4 8 9
//    //struct ListNode* cur = newhead;
//    //while (cur)
//    //{
//    //    printf("%d ", cur->val);
//    //    cur = cur->next;
//    //}
//    return 0;
//}




/**
 * Note: The returned array must be malloced, assume caller calls free().
 */

//void Swap(int* p, int* c)
//{
//	int tmp = *p;
//	*p = *c;
//	*c = tmp;
//}
//
//int GetMid(int* a, int left, int right)
//{
//	//int midi = (left + right) / 2;
//	int midi = left + (rand() % (right - left));
//	if (a[left] < a[midi])
//	{
//		if (a[midi] < a[right])
//		{
//			return midi;
//		}
//		else if (a[left] > a[right])
//		{
//			return left;
//		}
//		else
//		{
//			return right;
//		}
//	}
//	else  // a[left] > a[midi]
//	{
//		if (a[midi] > a[right])
//		{
//			return midi;
//		}
//		else if (a[left] < a[right])
//		{
//			return left;
//		}
//		else
//		{
//			return right;
//		}
//	}
//}
//
//int Partsort(int* a, int left, int right)
//{
//	int midi = GetMid(a, left, right);
//	Swap(&a[midi], &a[left]);
//	int cur = left + 1;
//	int key = a[left];
//	while (cur <= right)
//	{
//		if (a[cur] < key)
//		{
//			Swap(&a[cur], &a[left]);
//			cur++;
//			left++;
//		}
//		else if (a[cur] > key)
//		{
//			Swap(&a[cur], &a[right]);
//			right--;
//		}
//		else
//		{
//			cur++;
//		}
//	}
//	return left;
//}
//
//void QuickSort(int* a, int begin, int end)
//{
//	if (begin >= end)
//	{
//		return;
//	}
//	//int keyi = Partsort(a, begin, end);
//
//	int left = begin, right = end;
//	int midi = GetMid(a, left, right);
//	Swap(&a[midi], &a[left]);
//	int cur = left + 1;
//	int key = a[left];
//	while (cur <= right)
//	{
//		if (a[cur] < key)
//		{
//			Swap(&a[cur], &a[left]);
//			cur++;
//			left++;
//		}
//		else if (a[cur] > key)
//		{
//			Swap(&a[cur], &a[right]);
//			right--;
//		}
//		else
//		{
//			cur++;
//		}
//	}
//	int keyi = left;
//	QuickSort(a, begin, keyi - 1);
//	QuickSort(a, keyi + 1, end);
//}
//
//int* sortArray(int* nums, int numsSize, int* returnSize)
//{
//	srand(time(0));
//	QuickSort(nums, 0, numsSize - 1);
//	*returnSize = numsSize;
//	return nums;
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,1,2,3 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int returnsize = 0;
//	sortArray(arr, sz, &returnsize);
//	for (int i = 0; i < returnsize; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	while (*(src-1))
//	{
//		*dest++ = *src++;
//	}
//	*dest = *src;
//	return ret;
//}

//	while (*src)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}

//int main()
//{
//	char arr1[20] = "hello";
//	char arr2[20] = { 0 };
//	memset(arr2, 1, 20);
//	my_strcpy(arr2, arr1);
//	printf("%s", arr2);
//}




//
//typedef  struct
//{
//	char  num[5];    //编号
//	char  name[9];   //姓名
//	char  sex[3];     //性别
//	char  phone[13];  //电话
//	char  addr[31];   //地址
//}DataType;
//
//typedef struct node
//{
//	DataType data;
//	struct node* next;
//}ListNode;
//
////头节点用head
////定义一个指向结点的指针变量 ListNode* p;
//
//ListNode* BuyListNode(DataType x);
//
//void ListNodePush(ListNode** phead);
//
//DataType Buynewdata();
//
//void ListNodePrint(ListNode** phead);
//
//int ListNodeFind(ListNode* head, const char* Findname);
//
//void ListNodePop(ListNode** phead, const char* Popname);
//
//ListNode* BuyListNode(DataType x)
//{
//	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
//	if (newnode == NULL)
//	{
//		perror("malloc fail");
//		return NULL;
//	}
//	newnode->next = NULL;
//	newnode->data = x;
//	return newnode;
//}
//
//DataType Buynewdata()
//{
//	DataType newdata;
//	printf("请依次输入编号 姓名 性别 电话 地址\n");
//	scanf("%s %s %s %s %s",
//		newdata.num, newdata.name, newdata.sex, newdata.phone, newdata.addr);
//	return  newdata;
//}
//
//void ListNodePush(ListNode** phead)
//{
//	assert(phead);
//	ListNode* newnode = BuyListNode(Buynewdata());
//	if (*phead == NULL)
//	{
//		*phead = newnode;
//	}
//	else
//	{
//		newnode->next = *phead;
//		*phead = newnode;
//	}
//}
//
//void ListNodePrint(ListNode** phead)
//{
//	ListNode* cur = *phead;
//	printf("%-5s%-10s%-8s%-13s%-31s\n",
//		"编号", "姓名", "性别", "电话", "地址");
//	while (cur)
//	{
//		printf("%-5s%-10s%-8s%-13s%-31s\n",
//			cur->data.num, cur->data.name, cur->data.sex,
//			cur->data.phone, cur->data.addr);
//		cur = cur->next;
//	}
//}
//
//int ListNodeFind(ListNode* head, const char* Findname)
//{
//	ListNode* cur = head;
//	while (cur)
//	{
//		if (strcmp(Findname, cur->data.name) == 0)
//		{
//			printf("找到了,该人的信息如下\n");
//			printf("%-5s%-10s%-8s%-13s%-31s\n",
//				"编号", "姓名", "性别", "电话", "地址");
//			printf("%-5s%-10s%-8s%-13s%-31s\n",
//				cur->data.num, cur->data.name, cur->data.sex,
//				cur->data.phone, cur->data.addr);
//			return 1;
//		}
//		else
//		{
//			cur = cur->next;
//		}
//	}
//	printf("找不到信息\n");
//	return 0;
//}
//
//void ListNodePop(ListNode** phead, const char* Popname)
//{
//	assert(*phead);
//	assert(phead);
//	if (ListNodeFind(*phead, Popname))
//	{
//		ListNode* Findnode = *phead;
//		ListNode* prev = *phead;
//		while (strcmp(Findnode->data.name, Popname) != 0)
//		{
//			prev = Findnode;
//			Findnode = Findnode->next;
//		}
//		prev->next = Findnode->next;
//		free(Findnode);
//		Findnode = NULL;
//		printf("删除该人信息成功\n");
//		return;
//	}
//	else
//	{
//		printf("找不到该人信息\n");
//		return;
//	}
//}
//
//void menu()
//{
//	printf("*********************************************\n");
//	printf("****1.建立信息表************** 2.插入信息 ****\n");
//	printf("****3.查询信息  ************** 4.删除信息 ****\n");
//	printf("****5.打印信息  ************** 0.退出     ****\n");
//	printf("*********************************************\n");
//}
//
//void SetupListNode(ListNode** phead)
//{
//	int num = 0;
//	printf("建立链表成功，开始录入信息\n");
//	printf("输入要录取信息的个数->");
//	scanf("%d", &num);
//	while (num--)
//	{
//		ListNodePush(phead);
//	}
//}
//
//void FindFunction(ListNode* head)
//{
//	char Findname[20] = { 0 };
//	printf("输入要查找的人名");
//	scanf("%s", Findname);
//	ListNodeFind(head, Findname);
//}
//
//void PopFunction(ListNode** phead)
//{
//	char Popname[20] = { 0 };
//	printf("输入要查找的人名");
//	scanf("%s", Popname);
//	ListNodePop(phead, Popname);
//}
//
//int main()
//{
//	menu();
//	int input = 0;
//	ListNode* head = NULL;
//	do
//	{
//		printf("请选择->");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			SetupListNode(&head);
//			break;
//		case 2:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				ListNodePush(&head);
//				break;
//			}
//		case 3:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				FindFunction(head);
//				break;
//			}
//		case 4:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				PopFunction(&head);
//				break;
//			}
//		case 5:
//			if (head == NULL)
//			{
//				printf("通讯录还未建立，请先建立\n");
//				break;
//			}
//			else
//			{
//				ListNodePrint(&head);
//				break;
//			}
//		case 0:
//			break;
//		default:
//			printf("请重新选择\n");
//			break;
//		}
//		menu();
//	} while (input);
//	return 0;
//}