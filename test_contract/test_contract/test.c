﻿#define _CRT_SECURE_NO_WARNINGS

#include "contact.h"

void menu()
{
	printf("**************************\n");
	printf("***1.add       2.del   ***\n");
	printf("***3.search    4.modify***\n");
	printf("***5.show      6.clean ***\n");
	printf("***7.order     0.exit  ***\n");
	printf("**************************\n");
}

int main()
{
	contact con;
	//加载文件信息到通讯录
	initcon(&con);
	loadcontact(&con);
	int input = 0;
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case add:
			addcontact(&con);
			break;
		case del:
			delcontact(&con);
			break;
		case search:
			searchcontact(&con);
			break;
		case modify:
			break;
		case show:
			showcontact(&con);
			break;
		case clean:
			cleancontact(&con);
			break;
		case order:
			ordercontact(&con);
			break;
		case Exit:
			Savecontact(&con); 
			Destroycontact(&con);
			break;
		default:
			break;
		}
	} while (input);
	return 0;
}