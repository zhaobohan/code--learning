﻿#pragma once
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define Max 100

typedef struct peoinfo
{
	char name[20];
	char sex[10];
	int age;
	char tele[30];
	char addr[20];
}peoinfo;

typedef struct contact
{
	peoinfo* data;
	int sz;
	int capacity;
}contact, * pcontact;

enum option
{
	Exit,
	add,
	del,
	search,
	modify,
	show,
	clean,
	order
};

void initcon(contact* pc);

void Destroycontact(contact* pc);

void addcontact(contact* pc);

void showcontact(const contact* pc);

void delcontact(contact* pc);

int find_name(contact* pc, const char* p);

void searchcontact(contact* pc);

void cleancontact(contact* pc);

void ordercontact(contact* pc);

void Savecontact(contact* pc);

void loadcontact(contact* pc);