﻿#define _CRT_SECURE_NO_WARNINGS

#include "contact.h"

//静态的版本
//void initcon(contact* pc)
//{
//	memset(pc->data, 0, sizeof(pc->data));
//	pc->sz = 0;
//}

//动态的版本
void initcon(contact* pc)
{
	pc->data = (peoinfo*)malloc(3 * sizeof(peoinfo));
	if (pc->data == NULL)
	{
		printf("初始化失败%s\n", strerror(errno));
		return;
	}
	pc->sz = 0;
	pc->capacity = 3;
}

void Destroycontact(contact* pc)
{
	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;
}

void Checkcapcity(contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		peoinfo* ptr = realloc(pc->data, (pc->capacity + 2) * sizeof(peoinfo));
		if (ptr == NULL)
		{
			printf("Checkcapcity:%s\n", strerror(errno));
			return;
		}
		else
		{
			pc->data = ptr;
			pc->capacity += 2;
			printf("增容成功,当前容量%d\n", pc->capacity);
		}
	}
}

void addcontact(contact* pc)
{
	Checkcapcity(pc);
	printf("输入姓名>");
	scanf("%s", pc->data[pc->sz].name);
	printf("输入性别>");
	scanf("%s", pc->data[pc->sz].sex);
	printf("输入年龄>");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("输入电话>");
	scanf("%s", pc->data[pc->sz].tele);
	printf("输入地址>");
	scanf("%s", pc->data[pc->sz].addr);
	pc->sz++;
}

void showcontact(const contact* pc)
{
	printf("%-7s%-5s%-5s%-5s%-5s\n",
		"姓名", "性别", "年龄", "电话", "地址");
	for (int i = 0; i < pc->sz; i++)
	{
		printf("%-7s%-5s%-5d%-5s%-5s\n",
			pc->data[i].name,
			pc->data[i].sex,
			pc->data[i].age,
			pc->data[i].tele,
			pc->data[i].addr);
	}
}

int find_name(contact* pc, const char* p)
{
	for (int i = 0; i < pc->sz; i++)
	{
		if (strcmp(pc->data[i].name, p) == 0)
		{
			return i;
		}
	}
	return -1;
}

void delcontact(contact* pc)
{
	char findname[20];
	printf("输入要删除的名字>");
	scanf("%s", findname);
	int pos = find_name(pc, findname);
	if (pos == -1)
	{
		printf("没找到该名字信息\n");
		return;
	}
	for (int i = pos; i < pc->sz - 1; i++)
	{
		pc->data[i] = pc->data[i + 1];
		pc->sz--;
	}
	printf("删除成功\n");
}

void searchcontact(contact* pc)
{
	char findname[20];
	printf("输入要查找的名字>");
	scanf("%s", findname);
	int pos = find_name(pc, findname);
	if (pos == -1)
		printf("没找到\n");
	else
		printf("找到了\n");
}

void cleancontact(contact* pc)
{
	initcon(pc);
}

int cmp_byname(const void* e1, const void* e2)
{
	return strcmp(((peoinfo*)e1)->name, ((peoinfo*)e2)->name);
}

void ordercontact(contact* pc)
{
	qsort(pc->data, pc->sz, sizeof(peoinfo), cmp_byname);
	printf("排序完成\n");
}

void Savecontact(contact* pc)
{
	FILE* pf = fopen("contact.dat", "wb");
	if (pf == NULL)
	{
		perror("Savecontact::fopen");
		return;
	}
	for (int i = 0; i < pc->sz; i++)
	{
		fwrite(pc->data + i, sizeof(struct peoinfo), 1, pf);
	}
	fclose(pf);
	pf = NULL;
	printf("保存成功\n");
}

void loadcontact(contact* pc)
{
	//打开文件 读文件 关闭文件
	FILE* pf = fopen("contact.dat", "rb");
	if (pf == NULL)
	{
		perror("loadcontact::fopen");
		return;
	}
	peoinfo tmp = { 0 };
	while (fread(&tmp, sizeof(peoinfo), 1, pf))
	{
		Checkcapcity(pc);
		pc->data[pc->sz] = tmp;
		pc->sz++;
	}
	fclose(pf);
	pf = NULL;
}