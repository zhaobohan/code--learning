#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

struct stu_info 
{
	char  stuNo[15];     //学号
	char  name[20];   //姓名
	char  sex[3];     //性别
	char  domNum[10];   //宿舍号 
	char  tel[12];        //电话号码 
};   //学生基本信息结构体

struct stu_grade 
{
	char  stuNo[15];     //学号
	char  courseNo[10];  //课程编号
	char  courseName[20];  //课程名称
	int  creditHour;  //学分
	int  triGrade;  //平时成绩
	int  experiGrade;  //实验成绩
	int  examGrade;  //卷面成绩
	float  totalGrade;  //综合成绩
	float  finalCreHour;  //实得学分
};   //学生成绩基本信息结构体

struct stu_info stu[80];
int LEN = sizeof(struct stu_info);

struct stu_grade stu2[80];
int LEN2 = sizeof(struct stu_grade);

void menu()
{
	printf("***********************************************\n");
	printf("*** 1.录入学生信息          2.搜索学生信息  ***\n");
	printf("*** 3.删除基本信息          4.删除成绩信息  ***\n");
	printf("****5.展示基本信息          6.展示成绩信息  ***\n");
	printf("****7.学生信息排序          0.退出管理系统  ***\n");
	printf("***********************************************\n");
}

void showInfor()
{
	FILE* fp1;
	int i, m = 0;
	fp1 = fopen("A.txt", "rb");
	while (!feof(fp1))
	{
		if (fread(&stu[m], LEN, 1, fp1) == 1)
			m++;
	}
	fclose(fp1);
	printf("学号\t姓名\t性别\t宿舍号\t电话号\t\n");
	for (i = 0; i < m; i++)
	{
		printf("%-8s%-8s%-8s%-8s%-8s\n",
			stu[i].stuNo,stu[i].name,stu[i].sex,stu[i].domNum,stu[i].tel);
	}
}

void showGrade()
{
	FILE* fp2;
	int i, m = 0;
	fp2 = fopen("B.txt", "rb");
	while (!feof(fp2))
	{
		if (fread(&stu2[m], LEN2, 1, fp2) == 1)
			m++;
	}
	fclose(fp2);
	printf("学号\t课程编号\t课程名称\t学分\t平时成绩\t实验成绩\t卷面成绩\t综合成绩\t实得学分\t\n");
	for (i = 0; i < m; i++)
	{
		printf("%-8s%-8s%-8s%-8d%-8d%-8d%-8d%-8f%-8f\n",
			stu2[i].stuNo,stu2[i].courseNo,stu2[i].courseName,stu2[i].creditHour,
		stu2[i].triGrade,stu2[i].experiGrade,stu2[i].examGrade,stu2[i].totalGrade,stu2[i].finalCreHour);
	}
}

void Add()
{
	int i, m = 0,n=0;
	char ch[2]={'\0'};
	printf("进行学生基本信息录入:\n");
	FILE* fp1;
	FILE* fp2;
	if ((fp1 = fopen("A.txt", "wb")) == NULL)
	{
		printf("无法打开学生基本信息\n");
	}
	for (i = 0; i < m; i++)
	{
		fwrite(&stu[i], LEN, 1, fp1);
	}
	printf("请进行选择-->(y/n):");
	scanf("%s", ch);
	while (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
	{
		printf("学号:"); 
		scanf("%s", &stu[m].stuNo);
		printf("姓名:");
		scanf("%s", stu[m].name);
		printf("性别:");
		scanf("%s", &stu[m].sex);
		printf("宿舍号:");
		scanf("%s", &stu[m].domNum);
		printf("电话号码:"); 
		scanf("%s", &stu[m].tel);
		if (fwrite(&stu[m], LEN, 1, fp1) != 1)
		{
			printf("无法保存!"); 
		}
		else 
		{ 
			printf("%s 保存成功!\n", stu[m].name); 
			m++; 
		}
		printf("是否继续存储?(y/n):");
		scanf("%s", ch);
	}
	fclose(fp1);
	printf("进行学生成绩基本信息录入:\n");
	if ((fp2 = fopen("B.txt", "wb")) == NULL)
	{
		printf("无法打开学生成绩基本信息\n");
	}
	for (i = 0; i < n; i++)
	{
		fwrite(&stu2[i], LEN2, 1, fp2);
	}
	printf("是否进行信息录入(y/n):");
	scanf("%s", ch);
	while (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
	{
		printf("学号:");
		scanf("%s", &stu2[n].stuNo);
		printf("课程编号:");
		scanf("%s", stu2[n].courseNo);
		printf("课程名称:");
		scanf("%s", &stu2[n].courseName);
		printf("学分:");
		scanf("%d", &stu2[n].creditHour);
		printf("平时成绩:");
		scanf("%d", &stu2[n].triGrade);
		printf("实验成绩:");
		scanf("%d", &stu2[n].experiGrade);
		printf("卷面成绩:");
		scanf("%d", &stu2[n].examGrade);
		printf("综合成绩:");
		scanf("%f", &stu2[n].totalGrade);
		printf("实得学分:");
		scanf("%f", &stu2[n].finalCreHour);
		if (fwrite(&stu2[n], LEN2, 1, fp2) != 1)
		{
			printf("保存失败!");
			getch();
		}
		else
		{
			printf("%s 成绩保存成功!\n", stu[n].name);
			n++;
		}
		printf("是否继续录入?(y/n):");
		scanf("%s", ch);
	}
	fclose(fp2);

	printf("录入完毕!\n");
}

void DelInfo()
{
	FILE* fp1;
	FILE* fp2;
	char snum[20];
	int i, j, m = 0;
	char ch[2];

	if ((fp1 = fopen("A.txt", "r+")) == NULL)
	{
		printf("无法打开\n"); 
		return;
	}
	while (!feof(fp1))  
		if (fread(&stu[m], LEN, 1, fp1) == 1)
			m++;
	fclose(fp1);
	if (m == 0)
	{
		printf("无内容，请先添加!\n");
		return;
	}
	printf("请输入要删除的学号:");
	scanf("%s", &snum);
	for (i = 0; i < m; i++)
		if (strcmp(snum,stu[i].stuNo)==0)
			break;
	printf("找到了，是否删除?(y/n)");
	scanf("%s", ch);
	if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
		for (j = i; j < m; j++)
			stu[j] = stu[j + 1];
	m--;
	if ((fp1 = fopen("A.txt", "wb")) == NULL)
	{
		printf("无法打开文件\n"); 
		return;
	}
	for (j = 0; j < m; j++)
		if (fwrite(&stu[j], LEN, 1, fp1) != 1)
		{
			printf("无法存储!\n");
		}
	fclose(fp1);
	printf("删除成功!\n");
}

void DelGrade()
{
	FILE* fp2;
	int snum, i, j, n = 0;
	char ch[2];
	if ((fp2 = fopen("B.txt", "r+")) == NULL)
	{
		printf("无法打开\n"); 
		return;
	}
	while (!feof(fp2))  
		if (fread(&stu2[n], LEN2, 1, fp2) == 1) 
			n++;
	fclose(fp2);
	if (n == 0)
	{
		printf("找不到记录!\n");
		return;
	}
	printf("请输入要删除的学号:");
	scanf("%d", &snum);
	for (i = 0; i < n; i++)
		if (snum == stu2[i].stuNo)
			break;
	printf("找到了，是否删除?(y/n)");
	scanf("%s", ch);
	if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
		for (j = i; j < n; j++)
			stu[j] = stu[j + 1];
	n--;
	if ((fp2 = fopen("data.txt", "wb")) == NULL)
	{
		printf("无法打开文件\n"); 
		return;
	}
	for (j = 0; j < n; j++)
		if (fwrite(&stu2[j], LEN2, 1, fp2) != 1)
		{
			printf("无法存储!\n");
			getch();
		}
	fclose(fp2);
	printf("删除成功!\n");
}

void Order()
{
	int option = 0;
	printf("1.平时成绩         2.实验成绩\n");
	printf("3.卷面成绩         4.综合成绩\n");
	printf("请选择排序依据:-->");
	scanf("%d", &option);
	FILE* fp2;
	struct stu_grade t;
	int i = 0, j = 0, m = 0;
	if ((fp2 = fopen("B.txt", "r+")) == NULL)
	{
		printf("无法打开!\n");
		return;
	}
	while (!feof(fp2))
		if (fread(&stu2[m], LEN2, 1, fp2) == 1)
			m++;
	fclose(fp2);
	if (m == 0)
	{
		printf("无输入记录!\n");
		return;
	}
	if ((fp2 = fopen("B.txt", "wb")) == NULL)
	{
		printf("无法打开\n");
		return;
	}
	switch (option)
	{
	case 1:
		for (i = 0; i < m - 1; i++)
			for (j = i + 1; j < m; j++)
				if (stu2[j].triGrade > stu2[j+1].triGrade)
				{
					t = stu2[i];
					stu2[i] = stu2[j];
					stu2[j] = t;
				}
		break;
	case 2:
		for (i = 0; i < m - 1; i++)
			for (j = i + 1; j < m; j++)
				if (stu2[i].experiGrade <= stu2[j].experiGrade)
				{
					t = stu2[i]; 
					stu2[i] = stu2[j]; 
					stu2[j] = t;
				}
		break;
	case 3:
		for (i = 0; i < m - 1; i++)
			for (j = i + 1; j < m; j++)
				if (stu2[i].examGrade<= stu2[j].examGrade)
				{
					t = stu2[i]; stu2[i] = stu2[j]; stu2[j] = t;
				}
		break;
	case 4:
		for (i = 0; i < m - 1; i++)
			for (j = i + 1; j < m; j++)
				if (stu2[i].totalGrade <= stu2[j].totalGrade)
				{
					t = stu2[i]; stu2[i] = stu2[j]; stu2[j] = t;
				}
		break;
	default:
		printf("输入格式错误，请重新输入");
	}
	if ((fp2 = fopen("B.txt", "wb")) == NULL)
	{
		printf("无法打开\n");
		return;
	}
	for (i = 0; i < m; i++)
		if (fwrite(&stu2[i], LEN2, 1, fp2) != 1)
		{
			printf("%s 无法存储!\n");
		}
	fclose(fp2);
	printf("存储成功\n");
}

void Search()
{
	char str[20] = { '\0' };
	int num = 0;
	printf("1.学生信息查询      2.学生成绩查询");
	printf("请选择你的查询方式:-->");
	scanf("%d",&num);
	FILE* fp;
	int snum, i, m = 0;
	char ch[2];
	if ((fp = fopen("A.txt", "rb")) == NULL)
	{
		printf("无法打开\n"); 
		return;
	}
	while (!feof(fp))  
		if (fread(&stu[m], LEN, 1, fp) == 1)
			m++;
	fclose(fp);
	if (m == 0) 
	{
		printf("无信息记录!\n"); 
		return; 
	}
	if (num == 1)
	{
		int op = 0;
		printf("1.学号  2.姓名  3.宿舍号码");
		printf("请选择你的查询方式-->");
		scanf("%d", &op);
		switch (op)
		{
		case 1:
			printf("请输入要查找的学号:");
			scanf("%s", &str);
			for (i = 0; i < m; i++)
				if (strcmp(stu[i].stuNo, str) == 0)
				{
					printf("找到了，是否显示?(y/n)");
					scanf("%s", ch);
					if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
					{
						printf("%-8s%-8s%-8s%-8s%-8s%\n",
							stu[i].stuNo, stu[i].name, stu[i].sex, stu[i].domNum, stu[i].tel);
						break;
					}
				}
			if (i == m)
				printf("未找到该学生信息!\n");
			break;

		case 2:
			printf("请输入要查找的姓名:");
			scanf("%s", &str);
			for (i = 0; i < m; i++)
				if (strcmp(stu[i].name, str) == 0)
				{
					printf("找到了，是否显示?(y/n)");
					scanf("%s", ch);
					if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
					{
						printf("%-8s%-8s%-8s%-8s%-8s%\n",
							stu[i].stuNo, stu[i].name, stu[i].sex, stu[i].domNum, stu[i].tel);
						break;
					}
				}
			if (i == m)
				printf("未找到该学生信息!\n");
			break;

		case 3:
			printf("请输入要查找的宿舍号:");
			scanf("%s", &str);
			for (i = 0; i < m; i++)
				if (strcmp(stu[i].domNum, str) == 0)
				{
					printf("找到了，是否显示?(y/n)");
					scanf("%s", ch);
					if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
					{
						printf("%-8s%-8s%-8s%-8s%-8s%\n",
							stu[i].stuNo, stu[i].name, stu[i].sex, stu[i].domNum, stu[i].tel);
						break;
					}
				}
			if (i == m)
				printf("未找到该学生信息!\n");
			break;
		}
	}
	else if (num == 2)
	{
		char stunum[20];
		printf("请输入要查找的学号:");
		scanf("%s", &str);
		for (i = 0; i < m; i++)
			if (strcmp(stu[i].stuNo, str) == 0)
			{
				strcpy(stunum, stu[i].stuNo);
			}
		if (i == m)
			printf("未找到该学生信息!\n");
		FILE* fp2;
		int snum2, i2, m2 = 0;
		char ch2[2];
		if ((fp2 = fopen("B.txt", "rb")) == NULL)
		{
			printf("无法打开\n"); 
			return;
		}
		while (!feof(fp))  
			if (fread(&stu2[m2], LEN2, 1, fp2) == 1) 
				m2++;
		fclose(fp);
		if (m2 == 0) 
		{
			printf("无信息记录!\n");
			return; 
		}
		for (i = 0; i < m; i++)
			if (strcmp(stunum,stu2[i].stuNo)==0)
			{
				printf("find the student,show?(y/n)");
				scanf("%s", ch);
				if (strcmp(ch, "Y") == 0 || strcmp(ch, "y") == 0)
				{
					printf("课程名称\t实得学分\t\n");
					printf("%-8s-8f%",stu2[i].courseName, stu2[i].finalCreHour);
					break;
				}
			}
		if (i == m) 
			printf("无法找到该学生!\n");
	}
	else
		printf("输入格式错误，请重新输入\n");

}

int main()
{
	int input =1;
	menu();
	do
	{
		printf("请选择:-->");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			Add();
			break;
		case 2:
			Search();
			break;
		case 3:
			DelInfo();
			break;
		case 4:
			DelGrade();
			break;
		case 5:
			showInfor();
			break;
		case 6:
			showGrade();
			break;
		case 7:
			Order();
			break;
		case 0:
			break;
		default:
			printf("输入格式错误");
		}
	} while (input);
	return 0;
}