#define _CRT_SECURE_NO_WARNINGS

#include "stack.h"

//void test1()
//{
//	Stack st;
//	StackInit(&st);
//	StackPush(&st, 1);
//	StackPush(&st, 2);
//	StackPush(&st, 3);
//	StackPush(&st, 4);
//	StackPush(&st, 5);
//	printf("The quantity in the stack:%d\n", StackSize(&st));
//	while (!StackEmpty(&st))
//	{
//		printf("%d ", StackTop(&st));
//		StackPop(&st);
//	}
//	StackDestroy(&st);
//}

void push(Stack ps)
{
	ps._a[0] = 10;
	ps._top++;
}

void test2()
{
	Stack s={0,0,0};
	StackInit(&s);
	push(s);
	printf("%d", s._a[0]);
}

int main()
{
	//test1();
	test2();
	return 0;
}