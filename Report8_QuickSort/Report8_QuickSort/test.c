#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

void PrintArrey(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

int PartSort(int* a, int left, int right)
{
	int cur = left + 1;
	int prev = left;
	int keyi = left;
	while (cur <= right)
	{
		if (a[cur] < a[keyi])
		{
			++prev;
			Swap(&a[prev], &a[cur]);
		}
		cur++;
	}

	Swap(&a[prev], &a[keyi]);

	return prev;
}


void QuickSort(int* a, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}
	int keyi = PartSort(a, begin, end);

	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1, end);
}

void menu()
{
	printf("*****************************\n");
	printf("********* 1. Sort **********\n");
	printf("********* 0. exit  **********\n");
	printf("*****************************\n");
}

void CheckCapacity(int* a, int num, int* capacity)
{
	if (num > *capacity)
	{
		int* tmp = (int*)realloc(a, sizeof(int) * num);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		a = tmp;
		*capacity = num;
	}
	else
	{
		return;
	}
}

void Sort()
{
	int capacity = 10;
	int* a = (int*)malloc(sizeof(int) * capacity);
	if (a == NULL)
	{
		perror("malloc fail");
		return;
	}
	int num;
	printf("输入要输入元素的个数->");
	scanf("%d", &num);
	CheckCapacity(a, num, &capacity);
	printf("输入对应个数的元素->");
	for (int i = 0; i < num; i++)
	{
		scanf("%d", &a[i]);
	}
	QuickSort(a, 0, num - 1);
	printf("排序完成:\n");
	PrintArrey(a, num);
	free(a);
	a = NULL;
}

int main()
{
	int input = 0;
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			Sort();
			break;
		case 0:
			break;
		default:
			printf("输入错误 重新输入\n");
		}
	} while (input);
}