#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int removeElement(int* nums, int numsSize, int val) {
    for (int i = 0; i < numsSize; i++)
    {
        while (nums[i] == val)
        {
            if (i == numsSize - 1)
            {
                numsSize--;
                break;
            }
            for (int j = i; j < numsSize-1; j++)
            {
                nums[j] = nums[j + 1];
            }
            numsSize--;
        }
    }
    return numsSize;
}

int main()
{
    int arr[] = { 3,2,2,3 };
    int sz = 4;
    int len = removeElement(arr, sz, 3);
    printf("%d\n", len);
    for (int i = 0; i < len; i++)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}

//int removeElement(int* nums, int numsSize, int val) {
//    int dst = 0;
//    int src = 0;
//    while (dst <= numsSize)
//    {
//        //while (nums[src] != val && nums[dst] != val)
//        //{
//        //    src++;
//        //    dst++;
//        //}
//        if (nums[dst] != val)
//        {
//            nums[src] = nums[dst];
//            src++;
//            dst++;
//        }
//        else
//        {
//            while(nums[dst]==val)
//                dst++;
//        }
//    }
//    return src-1;
//}

