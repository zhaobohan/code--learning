#define _CRT_SECURE_NO_WARNINGS
#include <bits/stdc++.h>
using namespace std;

int FirstNotRepeatingChar(char* str)
{
	// write code here
	int pos = -1;
	int num = 0;
	for (int i = 0; str[i] != 0; i++)
	{
		for (int j = 0; str[j] != 0; j++)
		{
			if (str[i] == str[j])
			{
				num++;
			}
		}
		if (num==1)
		{
			pos = i;
			break;
		}
		num = 0;
	}
	return pos;
}

int main()
{
	char arr[] = { "google" };
	int ret = FirstNotRepeatingChar(arr);
	cout << ret;
	return 0;
}